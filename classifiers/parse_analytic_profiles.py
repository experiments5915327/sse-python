import os
import xml.etree.ElementTree as ET

class AnalyticProfile:
    def __init__(self, name, content_type, data_format, store_policy, priority, serialization_name, file_prefix_name, file_ext_name, binary_source_name=None, binary_source_index=None, xml_template=None):
        self.name = name
        self.content_type = content_type
        self.data_format = data_format
        self.store_policy = store_policy
        self.priority = priority
        self.serialization_name = serialization_name
        self.file_prefix_name = file_prefix_name
        self.file_ext_name = file_ext_name
        self.binary_source_name = binary_source_name
        self.binary_source_index = binary_source_index
        self.xml_template = xml_template

def parse_analytic_profile(file_path):
    tree = ET.parse(file_path)
    root = tree.getroot()

    actions = []
    for action in root.findall('Action'):
        name = action.find('Name').text
        content_type = action.find('ContentType').text
        data_format = action.find('DataFormat').text
        store_policy = int(action.find('StorePolicy').text)
        priority = int(action.find('Priority').text)
        serialization_name = action.find('SerializationName').text
        file_prefix_name = action.find('FilePrefixName').text
        file_ext_name = action.find('FileExtName').text
        binary_source_name = action.find('BinarySourceName').text if action.find('BinarySourceName') is not None else None
        binary_source_index = int(action.find('BinarySourceIndex').text) if action.find('BinarySourceIndex') is not None else None
        xml_template = action.find('XMLTemplate').text if action.find('XMLTemplate') is not None else None
        
        actions.append(AnalyticProfile(
            name, content_type, data_format, store_policy, priority, serialization_name,
            file_prefix_name, file_ext_name, binary_source_name, binary_source_index, xml_template
        ))
    return actions

def process_analytic_profiles(input_dir, output_dir):
    profiles = {}
    for subdir, _, files in os.walk(input_dir):
        for file in files:
            if file.endswith(".xml"):
                profile_path = os.path.join(subdir, file)
                actions = parse_analytic_profile(profile_path)
                profiles[file] = actions
    return profiles

if __name__ == "__main__":
    input_dir = "./AnalyticProfiles"
    output_dir = "./processed_analytic_profiles"
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    profiles = process_analytic_profiles(input_dir, output_dir)
    for profile_name, actions in profiles.items():
        print(f"Processed {profile_name}:")
        for action in actions:
            print(f"  - {action.name} ({action.content_type})")

how to build this thing:

docker build -t sse -f docker/Dockerfile .

Run (use a local or persistent directory for the saved stream configs):
docker run -d --name sse-container -p 8000:8000 -v /mnt/c/dev/equitus/sse-python:/app/config sse


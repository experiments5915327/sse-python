import unittest
import cv2
from sse.image_scaler import scale_image

class TestImageScaler(unittest.TestCase):
    def test_scale_image(self):
        image = cv2.imread('tests/test_image.jpg')  # Add a valid test image path
        width, height = 640, 480
        scaled_image = scale_image(image, width, height)
        self.assertEqual(scaled_image.shape[1], width)
        self.assertEqual(scaled_image.shape[0], height)

import unittest
from unittest.mock import patch, MagicMock
from queue import Queue
from sse.rtsp_stream_handler import RTSPStreamHandler

class TestRTSPStreamHandler(unittest.TestCase):
    @patch('cv2.VideoCapture')
    def test_rtsp_stream_handler(self, mock_video_capture):
        mock_video_capture.return_value.read.return_value = (True, 'frame')
        frame_queue = Queue()
        handler = RTSPStreamHandler('rtsp://example.com/stream', frame_queue)
        handler.start()
        handler._capture_loop()
        self.assertFalse(frame_queue.empty())
        handler.stop()

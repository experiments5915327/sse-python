import unittest
from unittest.mock import patch, MagicMock
from queue import Queue
from sse.object_detector import ObjectDetector
import asyncio
import cv2
import os

class TestObjectDetector(unittest.TestCase):
    @patch('sse.post_request.post_image')
    @patch('cv2.imwrite')
    def test_object_detector(self, mock_imwrite, mock_post_image):
        frame_queue = Queue()
        frame = cv2.imread('tests/test_image.jpg')  # Add a valid test image path
        frame_queue.put(frame)
        detector = ObjectDetector(frame_queue, 1.0, 'http://localhost:5000/detect/objects', 1)
        
        mock_post_image.return_value = asyncio.Future()
        mock_post_image.return_value.set_result({
            'results': [{
                'detections': [{
                    'bbox': {'x1': 50, 'y1': 50, 'x2': 100, 'y2': 100}
                }]
            }]
        })

        loop = asyncio.get_event_loop()
        loop.run_until_complete(detector.start())
        loop.run_until_complete(detector._detection_loop())
        
        mock_post_image.assert_called()
        mock_imwrite.assert_called()
        
        loop.run_until_complete(detector.stop())

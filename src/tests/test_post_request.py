import unittest
from unittest.mock import patch, MagicMock
import cv2
from sse.post_request import post_image
import asyncio

class TestPostRequest(unittest.TestCase):
    @patch('aiohttp.ClientSession.post')
    def test_post_image(self, mock_post):
        mock_response = MagicMock()
        mock_response.raise_for_status.return_value = None
        mock_response.json.return_value = asyncio.Future()
        mock_response.json.return_value.set_result({'status': 'OK'})
        mock_post.return_value.__aenter__.return_value = mock_response

        image = cv2.imread('tests/test_image.jpg')  # Add a valid test image path
        loop = asyncio.get_event_loop()
        response = loop.run_until_complete(post_image('http://localhost:5000/detect/objects', image))
        
        self.assertEqual(response, {'status': 'OK'})
        mock_post.assert_called_once()

import grpc
import sse_pb2
import sse_pb2_grpc
import argparse
from google.protobuf.empty_pb2 import Empty
import config

def list_sse(stub):
    response = stub.ListSSEs(Empty())
    for sse_info in response:
        print(f"SSE on port: {sse_info.port}")

def open_session(stub, port):
    session_request = sse_pb2.SessionRequest(port=port)
    session_handle = stub.OpenSession(session_request)
    print(f"Session opened with handle: {session_handle.handle}")

def close_session(stub, handle):
    session_handle = sse_pb2.SessionHandle(handle=handle)
    stub.CloseSession(session_handle, Empty())
    print(f"Session with handle {handle} closed")

def list_sessions(stub):
    response = stub.ListSessions(Empty())
    for session_info in response:
        print(f"Session handle: {session_info.handle}, host: {session_info.host}")

def close_sessions(stub):
    stub.CloseSessions(Empty())
    print("All sessions closed")

def list_engines(stub, handle):
    session_handle = sse_pb2.SessionHandle(handle=handle)
    response = stub.ListEngines(session_handle)
    for engine_summary in response:
        status = "RUNNING" if engine_summary.status.status == sse_pb2.EngineStatus.RUNNING else "STOPPED"
        print(f"Engine ID: {engine_summary.engine_id}, Status: {status}, Profile Name: {engine_summary.profile_name}")

def start_engines(stub, handle):
    session_handle = sse_pb2.SessionHandle(handle=handle)
    stub.StartEngines(session_handle, Empty())
    print(f"All engines started for session handle {handle}")

def stop_engines(stub, handle):
    session_handle = sse_pb2.SessionHandle(handle=handle)
    stub.StopEngines(session_handle, Empty())
    print(f"All engines stopped for session handle {handle}")

def restart_engines(stub, handle):
    session_handle = sse_pb2.SessionHandle(handle=handle)
    stub.RestartEngines(session_handle, Empty())
    print(f"All engines restarted for session handle {handle}")

def request_engine_status(stub, handle, engine_id):
    engine_request = sse_pb2.EngineInformationRequest(session=sse_pb2.SessionHandle(handle=handle), engine_id=engine_id)
    response = stub.RequestEngineStatus(engine_request)
    status = "RUNNING" if response.status == sse_pb2.EngineStatus.RUNNING else "STOPPED"
    print(f"Engine ID: {engine_id}, Status: {status}")

def request_resource_information_xml(stub, handle):
    session_handle = sse_pb2.SessionHandle(handle=handle)
    response = stub.RequestResourceInformationXML(session_handle)
    print(f"Resource Information XML: {response.resourceinfo}")

def request_sse_information_xml(stub, handle):
    session_handle = sse_pb2.SessionHandle(handle=handle)
    response = stub.RequestSSEInformationXML(session_handle)
    print(f"SSE Information XML: {response.sseinfo}")

def request_engine_information_xml(stub, handle, engine_id):
    engine_request = sse_pb2.EngineInformationRequest(session=sse_pb2.SessionHandle(handle=handle), engine_id=engine_id)
    response = stub.RequestEngineInformationXML(engine_request)
    print(f"Engine Information XML: {response.engineinfo}")

def create_engine(stub, handle, engine_info):
    create_request = sse_pb2.CreateEngineRequest(session=sse_pb2.SessionHandle(handle=handle), engine_info=sse_pb2.EngineInformationXML(engineinfo=engine_info))
    response = stub.CreateEngine(create_request)
    print(f"Engine creation result: {'Success' if response.result else 'Failure'}")

def delete_engine(stub, handle, engine_id):
    engine_request = sse_pb2.EngineActionRequest(session=sse_pb2.SessionHandle(handle=handle), engine_id=engine_id)
    response = stub.DeleteEngine(engine_request)
    print(f"Engine deletion result: {'Success' if response.result else 'Failure'}")

def request_metadata_video_image(stub, handle, engine_id):
    request = sse_pb2.RequestMetadataRequestVideoImage(session=sse_pb2.SessionHandle(handle=handle), engine_id=engine_id)
    response = stub.RequestMetadataVideoImage(request)
    print(f"Video Image Metadata: Width={response.width}, Height={response.height}, Frame={response.frame_number}, Timestamp={response.timestamp}")

def request_metadata_detected_object(stub, handle, engine_id):
    request = sse_pb2.RequestMetadataRequest(session=sse_pb2.SessionHandle(handle=handle), engine_id=engine_id)
    response = stub.RequestMetadataDetectedObject(request)
    print(f"Detected Object Metadata: Frame={response.frame}, Timestamp={response.timestamp}")

def check_file_path(stub, handle, file_path, is_relative, mode):
    request = sse_pb2.CheckFilePathRequest(session=sse_pb2.SessionHandle(handle=handle), file_path=file_path, is_relative=is_relative, mode=mode)
    response = stub.CheckFilePath(request)
    print(f"Check File Path Result: {'Exists' if response.result else 'Does not exist'}")

def main():
    parser = argparse.ArgumentParser(description="SSE CLI")
    parser.add_argument("operation", choices=["list_sse", "open_session", "close_session", "list_sessions",
                                              "close_sessions", "list_engines", "start_engines",
                                              "stop_engines", "restart_engines", "request_engine_status",
                                              "request_resource_information_xml", "request_sse_information_xml",
                                              "request_engine_information_xml", "create_engine", "delete_engine",
                                              "request_metadata_video_image", "request_metadata_detected_object",
                                              "check_file_path"])
    parser.add_argument("--port", type=int, help="Port for opening session")
    parser.add_argument("--handle", type=int, help="Session handle")
    parser.add_argument("--engine_id", type=int, help="Engine ID for operations")
    parser.add_argument("--engine_info", type=str, help="Engine information XML for creating engine")
    parser.add_argument("--file_path", type=str, help="File path to check")
    parser.add_argument("--is_relative", type=bool, help="Is file path relative")
    parser.add_argument("--mode", type=int, choices=[0, 1], help="File path access mode: 0 for READ, 1 for WRITE")

    args = parser.parse_args()

    sse_host = "10.16.1.102"
    sse_host_port = "60000"

    with grpc.insecure_channel(f'{sse_host}:{sse_host_port}') as channel:
        stub = sse_pb2_grpc.SSEStub(channel)

        if args.operation == "list_sse":
            list_sse(stub)
        elif args.operation == "open_session":
            if args.port is None:
                print("Please provide a port for opening session")
                return
            open_session(stub, args.port)
        elif args.operation == "close_session":
            if args.handle is None:
                print("Please provide a handle for closing session")
                return
            close_session(stub, args.handle)
        elif args.operation == "list_sessions":
            list_sessions(stub)
        elif args.operation == "close_sessions":
            close_sessions(stub)
        elif args.operation == "list_engines":
            if args.handle is None:
                print("Please provide a handle for listing engines")
                return
            list_engines(stub, args.handle)
        elif args.operation == "start_engines":
            if args.handle is None:
                print("Please provide a handle for starting engines")
                return
            start_engines(stub, args.handle)
        elif args.operation == "stop_engines":
            if args.handle is None:
                print("Please provide a handle for stopping engines")
                return
            stop_engines(stub, args.handle)
        elif args.operation == "restart_engines":
            if args.handle is None:
                print("Please provide a handle for restarting engines")
                return
            restart_engines(stub, args.handle)
        elif args.operation == "request_engine_status":
            if args.handle is None or args.engine_id is None:
                print("Please provide both handle and engine_id for requesting engine status")
                return
            request_engine_status(stub, args.handle, args.engine_id)
        elif args.operation == "request_resource_information_xml":
            if args.handle is None:
                print("Please provide a handle for requesting resource information")
                return
            request_resource_information_xml(stub, args.handle)
        elif args.operation == "request_sse_information_xml":
            if args.handle is None:
                print("Please provide a handle for requesting SSE information")
                return
            request_sse_information_xml(stub, args.handle)
        elif args.operation == "request_engine_information_xml":
            if args.handle is None or args.engine_id is None:
                print("Please provide both handle and engine_id for requesting engine information")
                return
            request_engine_information_xml(stub, args.handle, args.engine_id)
        elif args.operation == "create_engine":
            if args.handle is None or args.engine_info is None:
                print("Please provide both handle and engine_info for creating engine")
                return
            create_engine(stub, args.handle, args.engine_info)
        elif args.operation == "delete_engine":
            if args.handle is None or args.engine_id is None:
                print("Please provide both handle and engine_id for deleting engine")
                return
            delete_engine(stub, args.handle, args.engine_id)
        elif args.operation == "request_metadata_video_image":
            if args.handle is None or args.engine_id is None:
                print("Please provide both handle and engine_id for requesting video image metadata")
                return
            request_metadata_video_image(stub, args.handle, args.engine_id)
        elif args.operation == "request_metadata_detected_object":
            if args.handle is None or args.engine_id is None:
                print("Please provide both handle and engine_id for requesting detected object metadata")
                return
            request_metadata_detected_object(stub, args.handle, args.engine_id)
        elif args.operation == "check_file_path":
            if args.handle is None or args.file_path is None or args.is_relative is None or args.mode is None:
                print("Please provide handle, file_path, is_relative, and mode for checking file path")
                return
            check_file_path(stub, args.handle, args.file_path, args.is_relative, args.mode)

if __name__ == "__main__":
    main()

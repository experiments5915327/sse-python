import cv2
import logging
from datetime import datetime, timedelta
from .metadata_types import MetadataDescription, PropertyDescription, PropertyTypeFloat, PropertyTypeBool, PropertyTypeInt

logger = logging.getLogger(__name__)

class AlertRendererContext:
    def __init__(self, received, alert):
        self.received = received
        self.alert = alert

class AlertRenderer:
    def __init__(self, dwell_time_seconds=5.0, show_alert_name=True, canvas_width=1280, canvas_height=720):
        self.dwell_time_seconds = dwell_time_seconds
        self.show_alert_name = show_alert_name
        self.canvas_width = canvas_width
        self.canvas_height = canvas_height
        self.cur_contexts = []

    def describe(self):
        return "The alert renderer component displays alerts."

    def describe_properties(self):
        return {
            "dwell_time_seconds": PropertyDescription(
                Name="dwell_time_seconds",
                Type=PropertyTypeFloat,
                Description="How long to display each alert in seconds",
                Default="5.0"
            ),
            "show_alert_name": PropertyDescription(
                Name="show_alert_name",
                Type=PropertyTypeBool,
                Description="Whether to display the alert name",
                Default="true"
            ),
            "canvas_width": PropertyDescription(
                Name="canvas_width",
                Type=PropertyTypeInt,
                Description="Canvas width",
                Default="1280"
            ),
            "canvas_height": PropertyDescription(
                Name="canvas_height",
                Type=PropertyTypeInt,
                Description="Canvas height",
                Default="720"
            )
        }

    def purge_contexts(self, cur_time):
        self.cur_contexts = [
            context for context in self.cur_contexts
            if (cur_time - context.received).total_seconds() < self.dwell_time_seconds
        ]

    def process_frame(self, frame, metadata_map):
        input_channel = frame["config"]["inputs"][0]
        alerts = metadata_map.get(input_channel, [])
        for alert in alerts:
            self.cur_contexts.append(AlertRendererContext(datetime.now(), alert))

        self.purge_contexts(datetime.now())

        canvas = cv2.UMat(self.canvas_height, self.canvas_width, cv2.CV_8UC3)
        canvas.setTo((0, 0, 0))
        xoff, yoff = 0, 0
        for context in self.cur_contexts:
            key_image = context.alert["KeyImage"]
            rows, cols, _ = key_image.shape
            if xoff + rows >= self.canvas_width:
                break
            canvas[yoff:yoff+rows, xoff:xoff+cols] = key_image
            xoff += cols
        return canvas

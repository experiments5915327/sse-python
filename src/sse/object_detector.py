import asyncio
import cv2
import os
from datetime import datetime
from queue import Queue, Empty
from concurrent.futures import ThreadPoolExecutor
from threading import Thread
from .dle import DLEClient
from .logger import get_logger
from .config import IMAGE_WIDTH, IMAGE_HEIGHT, IMAGE_LOG_FOLDER, RATE_LIMIT
from .trackers.opencv_tracker import OpenCVTracker
from .image_scaler import scale_image

logger = get_logger(__name__)

# def scale_image(image: Any, width: int, height: int) -> Any:
#     """Rescale the image to the specified width and height."""
#     return cv2.resize(image, (width, height))

class ObjectDetector:
    def __init__(self, frame_queue: Queue, interval: float, dle_client: DLEClient, mils_client, stream_id: int, view_id: int):
        self.frame_queue = frame_queue
        self.interval = interval
        self.dle_client = dle_client
        self.mils_client = mils_client
        self.stream_id = stream_id
        self.view_id = view_id
        self.running = False
        self.incrementor = 0
        self.detection_task = None
        self.tracker = OpenCVTracker()  # Use the OpenCVTracker class
        self.executor = ThreadPoolExecutor(max_workers=4)
        self.processed_frame_count = 0

    async def start(self):
        self.running = True
        self.detection_task = asyncio.create_task(self._detection_loop())

    async def stop(self):
        self.running = False
        if self.detection_task:
            await self.detection_task

    async def _detection_loop(self):
        while self.running:
            try:
                frame = self.frame_queue.get_nowait()
                asyncio.create_task(self._process_frame(frame))
            except Empty:
                pass
            await asyncio.sleep(self.interval)

    async def _process_frame(self, frame):
        try:
            self.tracker.update_trackers(frame, self.incrementor)
            if self.incrementor % int(RATE_LIMIT / self.interval) == 0:
                detection_results = await self._rate_limited_post_image(frame)
                self._initialize_trackers(frame, detection_results)
            self.processed_frame_count += 1
            self.incrementor += 1
        except Exception as e:
            logger.error(f"Failed to process frame: {e}")

    async def _rate_limited_post_image(self, frame):
        detection_results = await self._call_detector_async(frame)
        await asyncio.sleep(RATE_LIMIT)  # Rate limiting
        return detection_results

    async def _call_detector_async(self, frame):
        loop = asyncio.get_running_loop()
        detection_results = await loop.run_in_executor(self.executor, self.dle_client.call_detector, "myimage.jpg", 95, frame)
        return detection_results

    def _initialize_trackers(self, frame, detection_results):
        for detection in detection_results:
            bbox = detection.rectangle
            x, y, w, h = bbox.min[0], bbox.min[1], bbox.max[0] - bbox.min[0], bbox.max[1] - bbox.min[1]
            self.tracker.add_tracker(frame, (x, y, w, h))
            self._save_frame_with_bounding_box(frame, x, y, x + w, y + h)
            cropped_image = frame[y:y + h, x:x + w]
            asyncio.create_task(self._scale_and_send_image(cropped_image))

    async def _scale_and_send_image(self, cropped_image):
        try:
            loop = asyncio.get_running_loop()
            resized_cropped_image = await loop.run_in_executor(self.executor, scale_image, cropped_image, IMAGE_WIDTH, IMAGE_HEIGHT)
            await self.dle_client.call_classifier_async("myimage.jpg", 95, resized_cropped_image)
            self.mils_client.send_image(resized_cropped_image, self.view_id)
        except Exception as e:
            logger.error(f"Failed to scale and send image: {e}")

    def _save_frame_with_bounding_box(self, frame, x1, y1, x2, y2, object_id=None):
        color = (0, 255, 0) if object_id is None else (0, 255, 255)
        cv2.rectangle(frame, (x1, y1), (x2, y2), color, 2)
        if object_id is not None:
            cv2.putText(frame, f"ID: {object_id}", (x1, y1 - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
        timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")
        filename = f"{self.stream_id}_{timestamp}_{self.incrementor}.jpg"
        filepath = os.path.join(IMAGE_LOG_FOLDER, filename)
        cv2.imwrite(filepath, frame)
        self.incrementor += 1

    def _send_event_to_mils(self, detection):
        event_data = {
            "type": "object_detected",
            "camera_number": self.stream_id,
            "timestamp": datetime.now().isoformat(),
            "detection": detection
        }
        self.mils_client.send_event(event_data)

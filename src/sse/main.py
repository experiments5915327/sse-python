import asyncio
import json
import os
from fastapi import FastAPI, HTTPException, Request, Body
from pydantic import BaseModel
from typing import List, Dict
from .config import (
    DETECTION_INTERVAL, DLE_PYTHON_URL, IMAGE_LOG_FOLDER, CONFIG_FILE_PATH,
    MILS_URL, MILS_USER, MILS_PASSWORD, MILS_LOCALE, MILS_TIMESTAMP_FORMAT,
    MILS_TIMEZONE, MILS_VERIFY_SSL, SSE_HOST, SSE_HOST_PORT, VIEW_ID, STATE, VFI
)
from .rtsp_stream_builder import RTSPStreamBuilder
from .mils_client import MILSClient
from .logger import get_logger
from .analytic_profile import load_analytic_profiles, load_track_data_fragments

logger = get_logger(__name__)

# Ensure the environment variable is correctly retrieved
ANALYTIC_PROFILES_DIR = os.getenv("ANALYTIC_PROFILES_DIR", "/app/AnalyticProfiles")

app = FastAPI()

# Middleware to log all requests
@app.middleware("http")
async def log_requests(request: Request, call_next):
    try:
        logger.info(f"Request: {request.method} {request.url}")

        params = dict(request.query_params)
        if params:
            logger.info(f"Request parameters: {params}")

        if request.method in ("POST", "PUT", "PATCH"):
            body = await request.body()
            if body:
                logger.info(f"Request body: {body.decode('utf-8')}")
    except Exception as e:
        logger.error(f"Error logging request: {e}")

    response = await call_next(request)

    try:
        logger.info(f"Response status: {response.status_code}")
    except Exception as e:
        logger.error(f"Error logging response: {e}")

    return response

# Ensure the image log directory exists
if not os.path.exists(IMAGE_LOG_FOLDER):
    os.makedirs(IMAGE_LOG_FOLDER)

# Ensure the config directory exists
config_dir = os.path.dirname(CONFIG_FILE_PATH)
if not os.path.exists(config_dir):
    os.makedirs(config_dir)

# Load analytic profiles
analytic_profiles = load_analytic_profiles(ANALYTIC_PROFILES_DIR)
logger.info(f"Loaded {len(analytic_profiles)} analytic profiles.")

# Initialize MILSClient
mils_client = MILSClient()

# Initialize RTSPStreamBuilder
stream_builder = RTSPStreamBuilder(mils_client)

def load_streams_from_config() -> List[Dict[str, any]]:
    if os.path.exists(CONFIG_FILE_PATH):
        with open(CONFIG_FILE_PATH, 'r') as f:
            return json.load(f)
    return []

def save_streams_to_config(streams: List[Dict[str, any]]):
    with open(CONFIG_FILE_PATH, 'w') as f:
        json.dump(streams, f)

async def register_sse():
    # Register SSE with MILS using environment variables for host and port
    if not mils_client.register_sse(SSE_HOST, SSE_HOST_PORT, STATE, VFI):
        logger.error("Failed to register SSE with MILS")
    else:
        logger.info("SSE registered with MILS successfully")

@app.on_event("startup")
async def startup_event():
    await register_sse()
    streams = load_streams_from_config()
    for stream in streams:
        stream_builder.add_stream(stream["rtsp_url"], DETECTION_INTERVAL, DLE_PYTHON_URL, stream["camera_number"])
    logger.info("SSE server started and streams initialized.")

@app.on_event("shutdown")
async def shutdown_event():
    await stream_builder.shutdown()
    logger.info("SSE server stopped.")

class StreamRequest(BaseModel):
    rtsp_url: str
    camera_number: int
    stream_id: int
    view_id: int

@app.post("/add_stream")
async def add_stream(stream_request: StreamRequest):
    try:
        logger.debug("Adding stream")
        stream_builder.add_stream(stream_request.rtsp_url, DETECTION_INTERVAL, DLE_PYTHON_URL, stream_request.camera_number, stream_request.stream_id, stream_request.view_id)
        streams = load_streams_from_config()
        logger.debug("Saving Stream Config")
        streams.append({"rtsp_url": stream_request.rtsp_url, "camera_number": stream_request.camera_number, "stream_id": stream_request.stream_id, "view_id": stream_request.view_id})
        save_streams_to_config(streams)
        return {"status": "Stream added successfully"}
    except Exception as e:
        logger.error(f"Error adding stream: {e}")
        raise HTTPException(status_code=500, detail="Error adding stream")

@app.delete("/remove_stream/{stream_id}")
async def remove_stream(stream_id: int):
    try:
        await stream_builder.remove_stream(stream_id)
        streams = load_streams_from_config()
        if stream_id >= len(streams) or stream_id < 0:
            raise HTTPException(status_code=404, detail="Stream not found")
        streams.pop(stream_id)
        save_streams_to_config(streams)
        return {"status": "Stream removed successfully"}
    except Exception as e:
        logger.error(f"Error removing stream: {e}")
        raise HTTPException(status_code=500, detail="Error removing stream")

@app.put("/vfi_job")
async def update_vfi_job(job_data: str = Body(...)):
    try:
        if mils_client.update_vfi_job(job_data):
            return {"status": "VFI job updated successfully"}
        else:
            raise HTTPException(status_code=500, detail="Failed to update VFI job")
    except Exception as e:
        logger.error(f"Error updating VFI job: {e}")
        raise HTTPException(status_code=500, detail="Error updating VFI job")

@app.post("/sse_system_xml")
async def create_sse_system_xml(xml_data: str = Body(...)):
    try:
        if mils_client.create_sse_system_xml(xml_data):
            return {"status": "SSE system XML created successfully"}
        else:
            raise HTTPException(status_code=500, detail="Failed to create SSE system XML")
    except Exception as e:
        logger.error(f"Error creating SSE system XML: {e}")
        raise HTTPException(status_code=500, detail="Error creating SSE system XML")

if __name__ == "__main__":
    import uvicorn
    uvicorn.run("sse.main:app", host="0.0.0.0", port=8000)

import xml.etree.ElementTree as ET
import os
from typing import List, Dict


class AnalyticAction:
    def __init__(self, name, content_type, data_format, store_policy, priority, serialization_name, file_prefix_name, file_ext_name, binary_source_name=None, binary_source_index=None, xml_template=None):
        self.name = name
        self.content_type = content_type
        self.data_format = data_format
        self.store_policy = store_policy
        self.priority = priority
        self.serialization_name = serialization_name
        self.file_prefix_name = file_prefix_name
        self.file_ext_name = file_ext_name
        self.binary_source_name = binary_source_name
        self.binary_source_index = binary_source_index
        self.xml_template = xml_template

class ComponentConfig:
    def __init__(self, component_id: int, name: str, type_id: int, friendly_name: str, description: str, configuration: Dict):
        self.component_id = component_id
        self.name = name
        self.type_id = type_id
        self.friendly_name = friendly_name
        self.description = description
        self.configuration = configuration


class AnalyticProfile:
    def __init__(self, name: str, supported_alerts: List[str], ui_flag: str, ingestion_tab_name: str,
                 blur_track_keyframe: bool, supported_resolutions: List[str], minimum_frame_rate: int,
                 track_filter: bool, mils_config: Dict, components: List[ComponentConfig]):
        self.name = name
        self.supported_alerts = supported_alerts
        self.ui_flag = ui_flag
        self.ingestion_tab_name = ingestion_tab_name
        self.blur_track_keyframe = blur_track_keyframe
        self.supported_resolutions = supported_resolutions
        self.minimum_frame_rate = minimum_frame_rate
        self.track_filter = track_filter
        self.mils_config = mils_config
        self.components = components

    def __repr__(self):
        return f"AnalyticProfile(name={self.name}, supported_alerts={self.supported_alerts})"

class TrackDataFragment:
    def __init__(self, view_id: str, track_id: str, timestamp: str, centroid: Dict[str, float], bbox: Dict[str, float], detected: bool):
        self.view_id = view_id
        self.track_id = track_id
        self.timestamp = timestamp
        self.centroid = centroid
        self.bbox = bbox
        self.detected = detected

    def __repr__(self):
        return f"TrackDataFragment(view_id={self.view_id}, track_id={self.track_id})"

def parse_analytic_profile(xml_data: str) -> AnalyticProfile:
    root = ET.fromstring(xml_data)
    if root.tag != 'AnalyticProfile':
        return None

    name = root.findtext('Name')

    supported_alert_list = root.find('SupportedAlertList')
    if supported_alert_list is not None:
        supported_alerts = [alert.text for alert in supported_alert_list.findall('AlertClass')]
    else:
        supported_alerts = []

    ui_flag = root.findtext('UIFlag')
    ingestion_tab_name = root.findtext('IngestionTabName')
    blur_track_keyframe = root.findtext('BlurTrackKeyFrame') == 'true'
    supported_resolutions = [res.text for res in root.findall('SupportedResolution')]
    minimum_frame_rate = int(root.findtext('MinimumFrameRate', 0))
    track_filter = root.findtext('TrackFilter') == 'true'

    mils_config = {}
    mils_config_group = root.find('MilsConfig')
    if mils_config_group is not None:
        for group in mils_config_group.findall('Group'):
            group_name = group.findtext('Name')
            parameters = {param.findtext('Name'): param.findtext('Value') for param in group.findall('Parameter')}
            mils_config[group_name] = parameters

    components = []
    for component in root.findall('Component'):
        comp_id = component.findtext('ID')
        comp_id = int(comp_id) if comp_id else None
        name = component.findtext('Name')
        type_id = component.findtext('Type')
        type_id = int(type_id) if type_id else None
        friendly_name = component.findtext('FriendlyName')
        description = component.findtext('Description')
        config = {}
        configuration = component.find('Configuration')
        if configuration is not None:
            for group in configuration.findall('Group'):
                group_name = group.findtext('Name')
                parameters = {param.findtext('Name'): param.findtext('Value') for param in group.findall('Parameter')}
                config[group_name] = parameters
        components.append(ComponentConfig(comp_id, name, type_id, friendly_name, description, config))

    return AnalyticProfile(name, supported_alerts, ui_flag, ingestion_tab_name, blur_track_keyframe,
                           supported_resolutions, minimum_frame_rate, track_filter, mils_config, components)

def parse_track_data_fragment(xml_data: str) -> TrackDataFragment:
    root = ET.fromstring(xml_data)
    view_id = root.findtext('viewId')
    track_id = root.findtext('trackId')
    timestamp = root.findtext('timestamp')
    centroid = {coord.tag: float(coord.text) for coord in root.find('centroid')}
    bbox = {coord.tag: float(coord.text) for coord in root.find('bBox')}
    detected = root.findtext('detected') == 'true'

    return TrackDataFragment(view_id, track_id, timestamp, centroid, bbox, detected)

def load_analytic_profiles(directory: str) -> List[AnalyticProfile]:
    profiles = []
    for filename in os.listdir(directory):
        if filename.endswith(".xml"):
            filepath = os.path.join(directory, filename)
            with open(filepath, 'r') as file:
                profile_data = file.read()
                profile = parse_analytic_profile(profile_data)
                if profile is not None:
                    profiles.append(profile)
    return profiles

def load_track_data_fragments(directory: str) -> List[TrackDataFragment]:
    fragments = []
    for filename in os.listdir(directory):
        if filename.endswith(".xml"):
            filepath = os.path.join(directory, filename)
            with open(filepath, 'r') as file:
                fragment_data = file.read()
                fragment = parse_track_data_fragment(fragment_data)
                fragments.append(fragment)
    return fragments

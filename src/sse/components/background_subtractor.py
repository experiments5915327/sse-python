import logging
import cv2
import numpy as np
from typing import Optional
from pydantic import BaseModel
from datetime import datetime

class Frame(BaseModel):
    sequence: int
    image: np.ndarray

class BackgroundSubtractorConfig(BaseModel):
    algorithm: str = "KNN"

class BackgroundSubtractor:
    ALGORITHM_MOG2 = "MOG2"
    ALGORITHM_KNN = "KNN"
    
    def __init__(self, config: BackgroundSubtractorConfig):
        self.algorithm = config.algorithm
        self.subtractor = self._create_subtractor()
    
    def _create_subtractor(self):
        if self.algorithm == self.ALGORITHM_MOG2:
            logger.info("Using MOG2 algorithm for background subtraction")
            return cv2.createBackgroundSubtractorMOG2()
        else:
            logger.info("Using KNN algorithm for background subtraction")
            return cv2.createBackgroundSubtractorKNN()

    def process_frame(self, frame: Frame) -> Optional[Frame]:
        start_time = datetime.now()
        logger.info(f"Subtracting background on image {frame.sequence}")
        
        try:
            mask = self.subtractor.apply(frame.image)
            dest = cv2.bitwise_and(frame.image, frame.image, mask=mask)
            frame.image = dest
            
            duration = (datetime.now() - start_time).total_seconds()
            logger.info(f"Processed frame {frame.sequence} in {duration:.4f} seconds")
            
            return frame
        except Exception as e:
            logger.error(f"Error processing frame {frame.sequence}: {e}")
            return None

    def close(self):
        pass
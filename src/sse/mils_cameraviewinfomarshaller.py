import xml.etree.ElementTree as ET
from xml.dom import minidom

class MILSCameraViewInfo:
    """
    A class to represent the camera view information for MILS.
    
    Attributes:
        view_id (str): The view ID of the camera.
        S3AnalyticProfileName (str): The name of the analytic profile.
        width (str): The width of the image resolution.
        height (str): The height of the image resolution.
        target_resolution (str): The target resolution.
        context (str): The context information.
        alert_definitions (list): A list of alert definitions.
    """
    def __init__(self, view_id, S3AnalyticProfileName, width, height, target_resolution, context, alert_definitions):
        self.view_id = view_id
        self.S3AnalyticProfileName = S3AnalyticProfileName
        self.width = width
        self.height = height
        self.target_resolution = target_resolution
        self.context = context
        self.alert_definitions = alert_definitions


class MILSCameraViewInfoMarshaller:
    """
    Handles marshalling of MILSCameraViewInfo objects to XML and from JSON.
    """
    def __init__(self):
        pass

    @staticmethod
    def _prettify(xml_str):
        """
        Returns a pretty-printed XML string.

        Args:
            xml_str (str): The XML string to prettify.
            
        Returns:
            str: Pretty-printed XML string.
        """
        parsed = minidom.parseString(xml_str)
        return parsed.toprettyxml(indent="  ")

    @staticmethod
    def to_xml(camera_view_infos):
        """
        Converts a list of MILSCameraViewInfo objects to an XML string.

        Args:
            camera_view_infos (list): List of MILSCameraViewInfo objects.
            
        Returns:
            str: XML string representation of camera view infos.
        """
        root = ET.Element("S3SSEConfiguration")

        for info in camera_view_infos:
            engine_elem = ET.SubElement(root, "Engine", viewID=str(info.view_id))

            # Add S3AnalyticProfileName component
            analytic_profile_component = ET.SubElement(engine_elem, "component", name="S3AnalyticProfileName")
            analytic_profile_group = ET.SubElement(analytic_profile_component, "group", name="CTDList")
            ET.SubElement(analytic_profile_group, "groupParameter").text = info.S3AnalyticProfileName

            # Add Index Writer component
            index_writer_component = ET.SubElement(engine_elem, "component", name="105E2C96-6203-11DB-8373-B622A1EF5492")
            image_resolution_group = ET.SubElement(index_writer_component, "group", name="Image Resolution")
            ET.SubElement(image_resolution_group, "parameter", name="Width").text = str(info.width)
            ET.SubElement(image_resolution_group, "parameter", name="Height").text = str(info.height)

            keyframe_options_group = ET.SubElement(index_writer_component, "group", name="KeyFrameOptions")
            ET.SubElement(keyframe_options_group, "parameter", name="TargetResolution").text = info.target_resolution

            # Add context
            context_elem = ET.SubElement(engine_elem, "context", name="Context")
            context_elem.text = info.context

            # Add alert definitions
            for alert in info.alert_definitions:
                alert_elem = ET.SubElement(engine_elem, "alertDefinition", alertType=alert["alertType"])
                alert_elem.set("alertMessage", alert["alertMessage"])

        xml_str = ET.tostring(root, encoding="unicode")
        return MILSCameraViewInfoMarshaller._prettify(xml_str)

    @staticmethod
    def from_json(json_data):
        """
        Converts JSON data to a list of MILSCameraViewInfo objects.

        Args:
            json_data (list): List of dictionaries representing the JSON data.
            
        Returns:
            list: List of MILSCameraViewInfo objects.
        """
        camera_view_infos = []

        for item in json_data:
            view_id = item.get("viewID")
            S3AnalyticProfileName = item.get("S3AnalyticProfileName")
            width = item.get("kfRes", {}).get("width")
            height = item.get("kfRes", {}).get("height")
            target_resolution = item.get("targetResolution")
            context = item.get("context")
            alert_definitions = item.get("alertDefinitions", [])

            camera_view_info = MILSCameraViewInfo(
                view_id=view_id,
                S3AnalyticProfileName=S3AnalyticProfileName,
                width=width,
                height=height,
                target_resolution=target_resolution,
                context=context,
                alert_definitions=alert_definitions
            )
            camera_view_infos.append(camera_view_info)
        return camera_view_infos
# metadata_types.py

from typing import List, Dict
import numpy as np

MetadataTypeAny = "any"
MetadataTypeImage = "image"
MetadataTypeScaledImage = "scaledImage"
MetadataTypeAlert = "alert"
MetadataTypeStatistic = "statistic"
MetadataTypeStatistic2D = "statistic2d"
MetadataTypeDetectedObject = "detectedObject"
MetadataTypeDetectedClass = "detectedClass"
MetadataTypeRegion = "region"
MetadataTypeDistance = "distance"
MetadataTypeTrack = "track"

PropertyTypeString = "string"
PropertyTypeBool = "bool"
PropertyTypeInt = "int"
PropertyTypeFloat = "float64"
PropertyTypeStringArray = "[]string"
PropertyTypeFloatMap = "map[string]float64"
PropertyTypeStringMap = "map[string]string"
PropertyTypePointArray = "[]Point"

PropertyTypeRegistry = {
    PropertyTypeString: str,
    PropertyTypeBool: bool,
    PropertyTypeInt: int,
    PropertyTypeFloat: float,
    PropertyTypeStringArray: List[str],
    PropertyTypeFloatMap: Dict[str, float],
    PropertyTypeStringMap: Dict[str, str],
    PropertyTypePointArray: List[np.array]
}

class MetadataDescription:
    def __init__(self, Type: str, Description: str, Optional: bool = False, Multiple: bool = False, Persisted: bool = False):
        self.Type = Type
        self.Description = Description
        self.Optional = Optional
        self.Multiple = Multiple
        self.Persisted = Persisted

class PropertyDescription:
    def __init__(self, Name: str, Type: str, Description: str, Default: str = "", Example: str = "", Required: bool = False):
        self.Name = Name
        self.Type = Type
        self.Description = Description
        self.Default = Default
        self.Example = Example
        self.Required = Required

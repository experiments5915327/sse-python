import cv2
import threading
import json
import asyncio
from queue import Queue, Full
from typing import List, Dict
from .logger import get_logger
from .object_detector import ObjectDetector
from .dle import DLEClient  # Import the DLEClient module
import time
from .config import IMAGE_WIDTH, IMAGE_HEIGHT, IMAGE_LOG_FOLDER, RATE_LIMIT

logger = get_logger(__name__)

class RTSPStreamHandler:
    def __init__(self, rtsp_url: str, frame_queue: Queue, stream_id: int, camera_number: int, queue_length: int, detector: ObjectDetector):
        self.rtsp_url = rtsp_url
        self.frame_queue = frame_queue
        self.stream_id = stream_id
        self.camera_number = camera_number
        self.cap = None
        self.running = False
        self.stopping = False  # Add a flag to indicate if stopping
        self.thread = None
        self.last_log_time = 0
        self.queue_length = queue_length
        self.frame_count = 0
        self.start_time = None
        self.detector = detector

    def start(self):
        if self.running:
            logger.warning(f"Stream {self.stream_id} is already running.")
            return
        self.running = True
        self.cap = cv2.VideoCapture(self.rtsp_url)
        if not self.cap.isOpened():
            logger.error(f"Failed to open stream: {self.rtsp_url}")
            self.running = False
            return
        self.thread = threading.Thread(target=self._capture_loop)
        self.thread.start()
        logger.info(f"Started stream {self.stream_id}: {self.rtsp_url} for camera {self.camera_number}")

    def stop(self):
        if not self.running:
            logger.warning(f"Stream {self.stream_id} is not running.")
            return
        self.running = False
        self.stopping = True  # Set stopping flag to true
        if self.thread:
            self.thread.join()  # Join the thread outside the loop
        if self.cap:
            self.cap.release()
        logger.info(f"Stopped stream {self.stream_id}")

    def _capture_loop(self):
        self.start_time = time.time()
        while self.running:
            ret, frame = self.cap.read()
            if not ret:
                logger.warning(f"Failed to read frame from stream {self.rtsp_url}")
                break

            self.frame_count += 1
            elapsed_time = time.time() - self.start_time
            incoming_fps = self.frame_count / elapsed_time
            processed_fps = self.detector.processed_frame_count / elapsed_time

            try:
                self.frame_queue.put(frame, timeout=1)
            except Full:
                current_time = time.time()
                if current_time - self.last_log_time > 60:  # Log at most once per minute
                    logger.info(f"Frame processing rate is {processed_fps:.2f} fps and the frame incoming rate is {incoming_fps:.2f} fps.")
                    self.last_log_time = current_time
                    # Reset the start time and frame count for accurate logging
                    self.start_time = current_time
                    self.frame_count = 0
                    self.detector.processed_frame_count = 0

        if not self.stopping:  # Avoid calling stop() if already stopping
            self.stop()

class RTSPStreamBuilder:
    def __init__(self, mils_client):
        self.mils_client = mils_client
        self.stream_handlers = []
        self.detectors = []

    def add_stream(self, rtsp_url, detection_interval, dle_url, camera_number, stream_id, view_id, queue_length=200):
        logger.info(f"Adding stream: {rtsp_url}")
        frame_queue = Queue(maxsize=queue_length)
        
        # Initialize DLEClient with URL
        dle_client = DLEClient(base_url=dle_url)
        dle_client.set_is_wvr(False)
        
        # Initialize ObjectDetector with DLEClient
        detector = ObjectDetector(frame_queue, detection_interval, dle_client, self.mils_client, stream_id, view_id)
        stream_handler = RTSPStreamHandler(rtsp_url, frame_queue, stream_id, camera_number, queue_length, detector)
        stream_handler.start()
        self.stream_handlers.append(stream_handler)

        asyncio.ensure_future(detector.start())
        self.detectors.append(detector)

    async def remove_stream(self, stream_id):
        if stream_id < 0 or stream_id >= len(self.stream_handlers):
            raise ValueError("Stream ID is out of range")

        self.stream_handlers[stream_id].stop()
        await self.detectors[stream_id].stop()

        self.stream_handlers.pop(stream_id)
        self.detectors.pop(stream_id)

    async def shutdown(self):
        logger.info(f"Shutdown signal.")
        for handler in self.stream_handlers:
            handler.stop()
        for detector in self.detectors:
            await detector.stop()

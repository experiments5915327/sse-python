import requests
import json
import time
from .config import (
    MILS_URL, MILS_USER, MILS_PASSWORD, MILS_LOCALE, 
    MILS_TIMESTAMP_FORMAT, MILS_TIMEZONE, MILS_VERIFY_SSL
)
from .logger import get_logger
import cv2
import uuid
from datetime import datetime
import socket
import xml.etree.ElementTree as et

logger = get_logger(__name__)

class MILSClient:
    MILS_CLIENT_PROTOCOL = "https"
    MILS_CLIENT_SESSION_API = "/milsng/admin/SVSProxy/session/?mime=json"
    MILS_CLIENT_ALERT_POST_API = "/milsng/admin/SVSProxy/alertsExternal/?mime=json"
    MILS_CLIENT_ALERT_KEYFRAME_POST_API = "/milsng/content/keyframe"
    MILS_CLIENT_CONFIG_API = "/milsng/admin/SVSProxy/sseConfigJob/"
    MILS_CLIENT_QUERY_API = "/milsng/content/sseSystemXml/"
    MILS_CLIENT_QUERY_SESSION_API = "/milsng/content/session/:gm?mime=xml&detailedstatus=true"
    MILS_CLIENT_REGISTER_SSE_API = "/milsng/RegisterSSE"
    MILS_CLIENT_UPDATE_VFI_JOB_API = "/milsng/admin/SVSProxy/vfiJob:updateJob?method=put&mime=xml"

    MILS_CLIENT_INDEX_ALERT_CONTENT_TYPE = "ae"
    MILS_CLIENT_FILES_PER_FOLDER_MAX = 500

    def __init__(self):
        self.session = requests.Session()
        self.session.headers.update({'accept': 'application/json'})
        self.logged_in = False
        self.query_logged_in = False
        self.view_id = None
        self.cur_day = -1
        self.folder_counter = 0
        self.file_counter = 0
        self.cookies = []

    def prepare_request(self, req, content_type, content_length=0):
        req.headers.update({
            "User-Agent": "IBM SSE",
            "Content-Type": content_type,
            "Connection": "close"
        })
        if content_length > 0:
            req.headers.update({"Content-Length": str(content_length)})

        for cookie in self.cookies:
            req.cookies.set(cookie.name, cookie.value)
            if cookie.name == "WVSESSION":
                req.headers.update({"WV-Origin": cookie.value})

    def perform_request(self, method, api, post_body=None, content_type="application/json"):
        url = f"{MILS_URL}{api}"
        req = requests.Request(method, url, data=post_body)
        self.prepare_request(req, content_type)
        prepared_req = self.session.prepare_request(req)

        try:
            response = self.session.send(prepared_req, verify=MILS_VERIFY_SSL)
            response.raise_for_status()
            return response.text
        except requests.RequestException as e:
            logger.error(f"Failed to perform request: {e}")
            return None

    def login(self):
        self.end_session()  # Ensure any existing session is ended
        session_post = {
            "items": [
                {
                    "userId": MILS_USER,
                    "password": MILS_PASSWORD,
                    "locale": MILS_LOCALE,
                    "mime": "json",
                    "timestampFormat": MILS_TIMESTAMP_FORMAT,
                    "numberFormat": "######.##",
                    "dateFormat": "yyy-MM-dd",
                    "timezone": MILS_TIMEZONE
                }
            ]
        }
        post_body = json.dumps(session_post)
        response = self.perform_request("POST", MILSClient.MILS_CLIENT_SESSION_API, post_body)
        if response:
            self.logged_in = True
            logger.info("Logged in to MILS successfully")
        else:
            logger.error("Login failed")

    def end_session(self):
        if self.logged_in:
            self.perform_request("DELETE", MILSClient.MILS_CLIENT_SESSION_API)
            self.logged_in = False
            self.cookies = []
            logger.info("Logged off from MILS successfully")

    def ensure_logged_in(self):
        if not self.logged_in:
            self.login()

    def get_fqdn(self):
        return socket.getfqdn()

    def authenticate_for_query(self):
        login_url = f"{MILS_URL}{MILSClient.MILS_CLIENT_QUERY_SESSION_API}"
        session_data = f"<content><session><userId>{MILS_USER}</userId><password>{MILS_PASSWORD}</password></session></content>"
        headers = {
            "Content-Type": "application/xml"
        }
        try:
            response = self.session.post(login_url, data=session_data, headers=headers, verify=MILS_VERIFY_SSL)
            if response.status_code == 200:
                self.session.headers.update({"Accept": "?mime=xml"})
                self.query_logged_in = True
                logger.info("Authenticated for MILS query successfully")
            else:
                logger.error(f"Failed to authenticate for MILS query: {response.text}")
        except requests.RequestException as e:
            logger.error(f"Exception during MILS query authentication: {e}")

    def construct_query_url(self, fqdn, port):
        query = f"(and(eq(sseIP,{fqdn}),eq(ssePort,{port})))"
        return f"{MILS_URL}{MILSClient.MILS_CLIENT_QUERY_API}{query}?pagesize=1&orderBy=timestamp,desc&itemcountmax=1"

    def is_sse_registered(self, fqdn, port):
        if not self.query_logged_in:
            self.authenticate_for_query()
        url = self.construct_query_url(fqdn, port)
        headers = {
            "User-Agent": "IBM SSE"
        }
        try:
            response = self.session.get(url, headers=headers, verify=MILS_VERIFY_SSL)
            if response.status_code == 200:
                logger.info(f"SSE registration query response {response.text}")
                result = response.json()
                if result.get("items"):
                    logger.info("SSE is already registered")
                    return True
                else:
                    logger.info("SSE is not registered")
                    return False
            else:
                logger.error(f"Failed to query SSE registration: {response.text}")
                return False
        except requests.RequestException as e:
            logger.error(f"Exception during SSE registration query: {e}")
            return False

    def register_sse(self, sse_host, sse_host_port, state, vfi):
        fqdn = self.get_fqdn()
        if self.is_sse_registered(fqdn, sse_host_port):
            logger.info("SSE is already registered, skipping registration")
            return True
        self.ensure_logged_in()
        if vfi is None:
            vfi = "0"

        url = f"{MILS_URL}{MILSClient.MILS_CLIENT_REGISTER_SSE_API}"
        params = {
            "view_id": "-1",
            "ip": sse_host,
            "port": sse_host_port,
            "state": state,
            "vfi": vfi
        }

        headers = {
            "User-Agent": "IBM SSE",
            "Content-Type": "application/x-www-form-urlencoded"
        }

        try:
            response = self.session.post(url, params=params, headers=headers, verify=MILS_VERIFY_SSL)
            if response.status_code == 200:
                logger.info("SSE registered successfully")
                return True
            else:
                logger.error(f"Failed to register SSE: {response.text}")
                return False
        except requests.RequestException as e:
            logger.error(f"Exception during SSE registration: {e}")
            return False

    def construct_config_url(self, fqdn, port):
        query = f"(and(eq(sseIP,{fqdn}),eq(ssePort,{port})))"
        return f"{MILS_URL}{MILSClient.MILS_CLIENT_CONFIG_API}{query}?pagesize=50&orderBy=created,asc&mine=xml"

    def get_sse_config_job(self, fqdn, port):
        self.ensure_logged_in()
        url = self.construct_config_url(fqdn, port)
        headers = {
            "User-Agent": "IBM SSE"
        }
        try:
            response = self.session.get(url, headers=headers, verify=MILS_VERIFY_SSL)
            if response.status_code == 200:
                logger.info(f"SSE config job query response {response.text}")
                result = et.fromstring(response.content)
                print(result)
            else:
                logger.error(f"Failed to query SSE config job response: {response.text}")
                return False
        except requests.RequestException as e:
            logger.error(f"Exception during SSE config job query: {e}")
            return False

    def update_vfi_job(self, job_data):
        self.ensure_logged_in()
        headers = {
            "Content-Type": "application/xml",
            "User-Agent": "IBM SSE"
        }
        try:
            response = self.session.put(f"{MILS_URL}{MILSClient.MILS_CLIENT_UPDATE_VFI_JOB_API}", data=job_data, headers=headers, verify=MILS_VERIFY_SSL)
            if response.status_code == 200:
                logger.info("VFI job updated successfully")
                return True
            else:
                logger.error(f"Failed to update VFI job: {response.text}")
                return False
        except requests.RequestException as e:
            logger.error(f"Exception during VFI job update: {e}")
            return False

    def create_sse_system_xml(self, xml_data):
        self.ensure_logged_in()
        headers = {
            "Content-Type": "application/xml",
            "User-Agent": "IBM SSE"
        }
        try:
            response = self.session.post(f"{MILS_URL}{MILSClient.MILS_CLIENT_QUERY_API}create", data=xml_data, headers=headers, verify=MILS_VERIFY_SSL)
            if response.status_code == 200:
                logger.info("SSE system XML created successfully")
                return True
            else:
                logger.error(f"Failed to create SSE system XML: {response.text}")
                return False
        except requests.RequestException as e:
            logger.error(f"Exception during SSE system XML creation: {e}")
            return False

    def post_binary_file(self, files, view_id):
        self.ensure_logged_in()
        boundary = "----WebKitFormBoundary7MA4YWxkTrZu0gWOuter"
        inner_boundary = "----WebKitFormBoundary7MA4YWxkTrZu0gWInner"
        multipart_data = []

        # Part 1
        multipart_data.append(f"--{boundary}\r\n")
        multipart_data.append(f"Content-Disposition: form-data; name=\"cid\"\r\n\r\n{files[0].m_pczFilePath}\r\n")
        multipart_data.append(f"--{boundary}\r\n")
        multipart_data.append(f"Content-Disposition: form-data; name=\"type\"\r\n\r\n{files[0].m_pczContentType}\r\n")
        multipart_data.append(f"--{boundary}\r\n")
        multipart_data.append(f"Content-Disposition: form-data; name=\"cpolicy\"\r\n\r\n{files[0].m_iMilsStoragePolicy}\r\n")
        multipart_data.append(f"--{boundary}\r\n")
        multipart_data.append(f"Content-Disposition: form-data; name=\"view_id\"\r\n\r\n{view_id}\r\n")
        multipart_data.append(f"--{boundary}\r\n")
        multipart_data.append(f"Content-Disposition: form-data; name=\"cpath\"\r\nContent-Type: multipart/mixed; boundary={inner_boundary}\r\n\r\n")

        # File parts
        for file in files:
            multipart_data.append(f"--{inner_boundary}\r\n")
            multipart_data.append(f"Content-Disposition: attachment; filename=\"{file.m_pczFileName}\"\r\n")
            multipart_data.append(f"Content-Type: image/jpeg\r\n")
            multipart_data.append(f"Content-Transfer-Encoding: binary\r\n\r\n")
            multipart_data.append(file.m_pcFile)

        multipart_data.append(f"\r\n--{inner_boundary}--\r\n")
        multipart_data.append(f"--{boundary}--\r\n")

        headers = {
            "Content-Type": f"multipart/form-data; boundary={boundary}"
        }

        # Join multipart data and encode to bytes
        data = b"".join(part.encode() if isinstance(part, str) else part for part in multipart_data)
        url = f"{MILS_URL}/milsng/content/keyframe"

        try:
            response = self.session.post(url, data=data, headers=headers, verify=MILS_VERIFY_SSL)
            if response.status_code == 200:
                logger.info("File posted successfully")
                return True
            else:
                logger.error(f"Failed to post file: {response.text}")
                return False
        except requests.RequestException as e:
            logger.error(f"Exception during file post: {e}")
            return False

    def send_image(self, image, view_id):
        """
        Sends a downscaled image to MILS.
        
        Args:
            image (numpy.ndarray): The image to send.
            view_id (int): The view ID for the MILS system.
        """
        file_data = self._prepare_file_data(image)
        self.post_binary_file([file_data], view_id)

    def _prepare_file_data(self, image):
        """
        Prepares the file data for sending to MILS.
        
        Args:
            image (numpy.ndarray): The image to prepare.
        
        Returns:
            dict: The prepared file data.
        """
        _, buffer = cv2.imencode('.jpg', image)
        file_data = buffer.tobytes()
        
        class CS3IngestFile:
            def __init__(self, file_data):
                self.m_pczFilePath = "image.jpg"
                self.m_pczContentType = "image/jpeg"
                self.m_iMilsStoragePolicy = 0
                self.m_pczFileName = "image.jpg"
                self.m_pcFile = file_data
                self.m_iFileSize = len(file_data)

        return CS3IngestFile(file_data)

    def send_event(self, event_data):
        """
        Sends an event to the MILS system.
        
        Args:
            event_data (dict): The event data to send.
        """
        self.ensure_logged_in()
        headers = {
            "Content-Type": "application/json",
            "WV-Origin": self.session.cookies.get('WVSESSION')
        }
        url = f"{MILS_URL}/milsng/event"
        
        try:
            response = self.session.post(url, headers=headers, json=event_data, verify=MILS_VERIFY_SSL)
            if response.status_code == 200:
                logger.info("Event sent successfully")
            else:
                logger.error(f"Failed to send event: {response.text}")
        except requests.RequestException as e:
            logger.error(f"Exception during event send: {e}")

    def create_alert_index(self, alert, image_height):
        centroid_index = {
            "X": alert["Centroid"]["X"],
            "Y": image_height - 1 - alert["Centroid"]["Y"]
        }
        bbox_index = {
            "LTX": alert["Bbox"]["Min"]["X"],
            "LTY": image_height - 1 - alert["Bbox"]["Min"]["Y"],
            "RBX": alert["Bbox"]["Max"]["X"],
            "RBY": image_height - 1 - alert["Bbox"]["Max"]["Y"]
        }
        key_frame_dir, key_frame_file = self.gen_key_frame_path("ae")
        key_frame_path = key_frame_dir + key_frame_file

        return {
            "ViewId": self.view_id,
            "AlertSummary": {
                "Identity": alert["Name"],
                "Class": "Region",
                "TrackId": "0",
                "Confidence": alert["Confidence"],
                "AuxInfo": alert["AuxInfo"]
            },
            "Start": {
                "Timestamp": self.get_time_value_string(alert["StartTime"]["TimeValue"]),
                "FrameNumber": alert["StartTime"]["FrameNumber"]
            },
            "End": {
                "Timestamp": self.get_time_value_string(alert["EndTime"]["TimeValue"]),
                "FrameNumber": alert["EndTime"]["FrameNumber"]
            },
            "KeyFrame": {
                "Display": 1,
                "Image": key_frame_path,
                "FrameTime": {
                    "Timestamp": self.get_time_value_string(alert["KeyFrameTime"]["TimeValue"]),
                    "FrameNumber": alert["KeyFrameTime"]["FrameNumber"]
                }
            },
            "AlertPriority": 64,
            "Archive": False,
            "ShowOnMils": alert["ShowOnMils"],
            "Transfer2Console": alert["Transfer2Console"],
            "AlertType": "S",
            "Centroid": centroid_index,
            "Bbox": bbox_index,
            "InitialAlertTimeStamp": {
                "Timestamp": self.get_time_value_string(alert["StartTime"]["TimeValue"]),
                "FrameNumber": alert["StartTime"]["FrameNumber"]
            },
            "VideoProxy": {
                "Path": "NONE",
                "Start": {
                    "Timestamp": "",
                    "FrameNumber": -1
                }
            },
            "keyFrameDir": key_frame_dir,
            "keyFrameFile": key_frame_file
        }

    def gen_key_frame_path(self, content_type):
        now = datetime.now()
        if self.cur_day < 0:
            self.cur_day = now.day
        elif self.cur_day != now.day:
            self.folder_counter = 0
            self.file_counter = 0
            self.cur_day = now.day

        ret_path = f"{now.year}\\{now.month}\\{now.day}\\{self.view_id}\\{content_type}\\{self.folder_counter}\\"
        ret_file = f"{content_type}{self.gen_unique_id()}-{self.folder_counter}-{self.file_counter}.jpg"

        self.file_counter += 1
        if self.file_counter > MILSClient.MILS_CLIENT_FILES_PER_FOLDER_MAX:
            self.folder_counter += 1
            self.file_counter = 0

        return ret_path, ret_file

    def gen_unique_id(self):
        return uuid.uuid4().hex

    def get_time_value_string(self, time_value):
        return time_value.strftime("%Y-%m-%dT%H.%M.%S.%f")[:-3]

    def send_alert_index(self, alert_index, image_height):
        alert_indices = {"Items": [alert_index]}
        post_body = json.dumps(alert_indices)
        url = f"{MILS_URL}{MILSClient.MILS_CLIENT_ALERT_POST_API}"
        try:
            response = self.session.post(url, data=post_body, headers={"Content-Type": "application/json"}, verify=MILS_VERIFY_SSL)
            if response.status_code == 200:
                logger.info("Alert index sent successfully")
                return True
            else:
                logger.error(f"Failed to send alert index: {response.text}")
                return False
        except requests.RequestException as e:
            logger.error(f"Exception during alert index send: {e}")
            return False

    def send_alert_keyframe(self, alert, alert_index):
        keyframe_path = f"{MILS_URL}{MILSClient.MILS_CLIENT_ALERT_KEYFRAME_POST_API}"
        data = {
            "cid": alert_index["KeyFrame"]["Image"],
            "type": MILSClient.MILS_CLIENT_INDEX_ALERT_CONTENT_TYPE,
            "cpolicy": "1",
            "view_id": str(self.view_id),
            "cpath": ""
        }
        _, buffer = cv2.imencode('.jpg', alert["KeyImage"])
        files = {"filename": ("keyframe.jpg", buffer.tobytes(), "image/jpeg")}
        try:
            response = self.session.post(keyframe_path, data=data, files=files, verify=MILS_VERIFY_SSL)
            if response.status_code == 200:
                logger.info("Alert keyframe sent successfully")
                return True
            else:
                logger.error(f"Failed to send alert keyframe: {response.text}")
                return False
        except requests.RequestException as e:
            logger.error(f"Exception during alert keyframe send: {e}")
            return False

    def send_alert(self, alert, image_height):
        if not self.logged_in:
            return "Do not have a session", None

        alert_index = self.create_alert_index(alert, image_height)
        if not self.send_alert_index(alert_index, image_height):
            return "Failed to send alert index", None

        if not self.send_alert_keyframe(alert, alert_index):
            return "Failed to send alert keyframe", None

        return "Alert sent successfully", alert_index

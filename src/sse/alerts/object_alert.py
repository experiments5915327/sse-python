import logging
from datetime import datetime
from typing import List, Dict, Any, Tuple
import numpy as np

logger = logging.getLogger(__name__)

class ObjectAlert:
    def __init__(self, config: Dict[str, Any]):
        """
        Initialize the ObjectAlert with configuration parameters.
        """
        self.config = config
        self.name = config.get("name", "")
        self.min_duration = config.get("min_duration", 0.0)
        self.min_count = config.get("min_count", 0)
        self.max_count = config.get("max_count", -1)
        self.object_labels = config.get("object_labels", [])
        self.key_frame_scale_factor = config.get("key_frame_scale_factor", 1.0)
        self.annotate_key_frames = config.get("annotate_key_frames", False)
        self.priority = config.get("priority", "")
        self.object_label_map = {label: True for label in self.object_labels}
        self.conditions_met = False
        self.alert_start_time = None
        self.alert_start_frame = -1

    def process_frame(self, frame: np.ndarray, timestamp: datetime, detected_objects: List[Dict[str, Any]]):
        """
        Process a frame to check if it meets the alert conditions.
        """
        logger.debug(f"Evaluating conditions for object alert in frame at {timestamp}")

        count = 0
        bounding_boxes = []

        # Count objects that match the specified labels
        for obj in detected_objects:
            label_found = self.object_label_map.get(obj['label']['name'], False)
            if not self.object_label_map or label_found:
                count += 1
                if self.annotate_key_frames:
                    bounding_boxes.append(obj['rectangle'])

        logger.debug(f"Found {count} object(s) satisfying object alert conditions")

        # Check if the object count is within the specified range
        if self.min_count <= count <= (self.max_count if self.max_count != -1 else float('inf')):
            if self.conditions_met:
                alert_duration = self.get_alert_duration(timestamp)
                if alert_duration >= self.min_duration:
                    alert = self.create_alert(frame, bounding_boxes, timestamp)
                    logger.info(f"Triggered alert for object count within range in frame at {timestamp}")
                    return alert
            else:
                self.record_alert_start(timestamp)
        else:
            self.cancel_alert_start()

        return None

    def record_alert_start(self, timestamp: datetime):
        """
        Record the start time of the alert when conditions are first met.
        """
        logger.debug("Recording object alert start")
        self.conditions_met = True
        self.alert_start_time = timestamp

    def get_alert_duration(self, current_time: datetime) -> float:
        """
        Calculate the duration for which the alert conditions have been met.
        """
        if self.alert_start_time is None:
            return 0.0
        return (current_time - self.alert_start_time).total_seconds()

    def cancel_alert_start(self):
        """
        Cancel the alert start if conditions are no longer met.
        """
        logger.debug("Canceling object alert start")
        if self.conditions_met:
            self.conditions_met = False
            self.alert_start_time = None

    def create_alert(self, frame: np.ndarray, bounding_boxes: List[Tuple[int, int, int, int]], timestamp: datetime) -> Dict[str, Any]:
        """
        Create an alert with relevant information.
        """
        alert = {
            "name": self.name,
            "priority": self.priority,
            "timestamp": timestamp,
            "bounding_boxes": bounding_boxes,
            "frame": frame,  # This can be the image frame or any other data associated with the alert
        }
        return alert

    def visualize(self, frame: np.ndarray):
        """
        Placeholder for visualization logic.
        """
        pass

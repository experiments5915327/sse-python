import cv2
import logging
import numpy as np
import asyncio
from datetime import datetime
from typing import List, Dict, Tuple, Any

from dle import DLEFilter, DLEFilterCrop, DLEClient

logger = logging.getLogger(__name__)

class TrackStat:
    def __init__(self, track_id: int, rectangle: Tuple[int, int, int, int], dle_filters: List[DLEFilter] = None):
        self.track_id = track_id
        self.enter_time = None  # Time when the object entered the region
        self.last_obs_time = None  # Last observed time of the object
        self.last_alert_time = None  # Last alert triggered time
        self.rectangle = rectangle  # Bounding box of the object
        self.cur_region = None  # Current region of the object
        self.dle_filters = dle_filters or []  # DLE filters applied to the object
        self.last_alert_time_dle_filter_aggregators = [
            [-1 for _ in range(len(dle_filter.aggregator_contexts))]
            for dle_filter in self.dle_filters
        ]

    def is_in_region(self) -> bool:
        return self.enter_time is not None

    def has_triggered_alert(self) -> bool:
        return self.last_alert_time is not None

    def get_area(self) -> int:
        x1, y1, x2, y2 = self.rectangle
        return (x2 - x1) * (y2 - y1)

    def get_duration_in_region_seconds(self) -> float:
        if self.last_obs_time is None or self.enter_time is None:
            return 0.0
        return (self.last_obs_time - self.enter_time).total_seconds()


class RegionAlert:
    def __init__(self, config: Dict[str, Any]):
        self.config = config
        self.name = config.get("name", "")
        self.part = config.get("part", "CENTROID")  # Part of the object to consider
        self.min_duration = config.get("min_duration", 0.0)  # Minimum duration in region to trigger alert
        self.min_obj_size = config.get("min_obj_size", 0)  # Minimum object size to trigger alert
        self.max_obj_size = config.get("max_obj_size", float('inf'))  # Maximum object size to trigger alert
        self.repeat_trigger = config.get("repeat_trigger", False)  # Allow repeated triggers
        self.repeat_trigger_frequency = config.get("repeat_trigger_frequency", 1.0)  # Frequency of repeated triggers
        self.priority = config.get("priority", "")
        self.show_on_mils = config.get("show_on_mils", True)
        self.transfer_to_console = config.get("transfer_to_console", False)
        self.key_frame_scale_factor = config.get("key_frame_scale_factor", 1.0)
        self.enable_visualizations = config.get("enable_visualizations", False)
        self.annotate_key_frames = config.get("annotate_key_frames", False)
        self.operator = config.get("operator", "AND")  # Operator for combining DLE filters
        self.regions = config.get("regions", [])
        self.dle_filter_configs = config.get("dle_filters", [])
        self.dle_filters_operator_and = self.operator.upper() == "AND"
        self.track_stats = {}  # Track statistics for each tracked object
        self.first_frame_time = None
        self.alert_count = 0
        self.part_pct = config.get("part_pct", 0.5)

        # Initialize DLE filters
        self.dle_filters = self.initialize_dle_filters(self.dle_filter_configs)

    def initialize_dle_filters(self, filter_configs: List[Dict[str, Any]]) -> List[DLEFilter]:
        filters = []
        for config in filter_configs:
            crop_config = config.get("crop", {})
            crop = DLEFilterCrop(crop_config.get("x1", 0.0), crop_config.get("y1", 0.0), crop_config.get("x2", 1.0), crop_config.get("y2", 1.0))
            dle_filter = DLEFilter(
                enable=config.get("enable", True),
                name=config.get("name", ""),
                target_labels=config.get("target_labels", []),
                url=config.get("url", ""),
                api_type_string=config.get("api_type_string", "detection"),
                rate=config.get("rate", 5.0),
                crop=crop,
                crop_jpeg_quality=config.get("crop_jpeg_quality", 95),
                aggregators=config.get("aggregators", [])
            )
            filters.append(dle_filter)
        return filters

    def process_frame(self, frame: np.ndarray, timestamp: datetime, detected_objects: List[Dict[str, Any]], track_ended_objects: List[int] = None):
        logger.debug(f"******* REGION ALERT FRAME at {timestamp} *******")

        if track_ended_objects:
            self.remove_track_stats(track_ended_objects)

        tracked_objects, track_stat_candidates = self.process_tracks(detected_objects, timestamp)
        self.update_dle_filters(frame, tracked_objects)
        self.trigger_alerts(track_stat_candidates, frame, timestamp)

    def process_tracks(self, detected_objects: List[Dict[str, Any]], timestamp: datetime):
        tracked_object_alert_candidates = []
        track_stat_candidates = []
        for obj in detected_objects:
            track_stat = self.get_track_stat(obj, timestamp)
            if track_stat:
                area = track_stat.get_area()
                candidate = (
                    track_stat.is_in_region() and
                    self.min_obj_size <= area <= self.max_obj_size and
                    track_stat.get_duration_in_region_seconds() >= self.min_duration
                )
                if candidate and not track_stat.dle_filters:
                    candidate = candidate and (
                        not track_stat.has_triggered_alert() or
                        (self.repeat_trigger and track_stat.get_duration_in_region_seconds() >= self.repeat_trigger_frequency)
                    )
                if candidate:
                    tracked_object_alert_candidates.append(obj)
                    track_stat_candidates.append(track_stat)
        return tracked_object_alert_candidates, track_stat_candidates

    def get_track_stat(self, track: Dict[str, Any], timestamp: datetime) -> TrackStat:
        region = self.get_region(track)
        if region is None:
            logger.debug(f"Track {track['identifier']} is not in a region")
            self.remove_track_stat(track['identifier'])
            return None
        if track['identifier'] in self.track_stats:
            track_stat = self.track_stats[track['identifier']]
            track_stat.rectangle = track['rectangle']
        else:
            track_stat = TrackStat(track['identifier'], track['rectangle'], self.dle_filters)
            self.track_stats[track['identifier']] = track_stat
        track_stat.cur_region = region
        if track_stat.enter_time is None:
            track_stat.enter_time = timestamp
        track_stat.last_obs_time = timestamp
        return track_stat

    def remove_track_stats(self, track_ended_objects: List[int]):
        for track_id in track_ended_objects:
            self.remove_track_stat(track_id)

    def remove_track_stat(self, track_id: int):
        if track_id in self.track_stats:
            del self.track_stats[track_id]

    def get_region(self, track: Dict[str, Any]):
        point = self.get_point(track)
        if point is None:
            return None
        for region in self.regions:
            if self.is_point_in_polygon(point, region):
                return region
        return None

    def get_point(self, track: Dict[str, Any]):
        x1, y1, x2, y2 = track['rectangle']
        if self.part == "CENTROID":
            return (int((x1 + x2) / 2), int((y1 + y2) / 2))
        elif self.part == "TOP":
            return (int((x1 + x2) / 2), y1)
        elif self.part == "BOTTOM":
            return (int((x1 + x2) / 2), y2)
        elif self.part == "ANY":
            return None
        return None

    def is_point_in_polygon(self, point: Tuple[int, int], polygon: List[Tuple[int, int]]):
        return cv2.pointPolygonTest(np.array(polygon, dtype=np.int32), point, False) >= 0

    def update_dle_filters(self, frame: np.ndarray, tracked_objects: List[Dict[str, Any]]):
        for tracked_object in tracked_objects:
            if track_stat := self.track_stats.get(tracked_object['identifier']):
                for dle_filter in track_stat.dle_filters:
                    dle_filter.update(frame, tracked_object)

    def trigger_alerts(self, track_stat_candidates: List[TrackStat], frame: np.ndarray, timestamp: datetime):
        for track_stat in track_stat_candidates:
            if self.check_dle_filters(track_stat):
                alert = self.create_alert(track_stat, frame, timestamp)
                self.alert_count += 1
                logger.info(f"Triggered alert for track {track_stat.track_id}")

    def check_dle_filters(self, track_stat: TrackStat) -> bool:
        if not track_stat.dle_filters:
            return True
        filter_results = []
        for dle_filter in track_stat.dle_filters:
            if any(context.is_triggered() for context in dle_filter.aggregator_contexts):
                filter_results.append(True)
            else:
                filter_results.append(False)
        if self.dle_filters_operator_and:
            return all(filter_results)
        else:
            return any(filter_results)

    def create_alert(self, track_stat: TrackStat, frame: np.ndarray, timestamp: datetime) -> Dict[str, Any]:
        alert = {
            "track_id": track_stat.track_id,
            "timestamp": timestamp,
            "rectangle": track_stat.rectangle
        }
        return alert

    def visualize(self, frame: np.ndarray):
        pass

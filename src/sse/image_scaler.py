from concurrent.futures import ThreadPoolExecutor
import logging
import cv2

logger = logging.getLogger(__name__)


def scale_image(image, width, height):
    try:
        scaled_image = cv2.resize(image, (width, height), interpolation=cv2.INTER_LANCZOS4)
        return scaled_image
    except Exception as e:
        logger.error(f"Error scaling image: {e}")
        return None

def scale_images_concurrently(images, width, height):
    with ThreadPoolExecutor() as executor:
        results = list(executor.map(lambda image: scale_image(image, width, height), images))
    return results

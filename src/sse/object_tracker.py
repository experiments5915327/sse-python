# object_tracker.py

import cv2
import logging
from datetime import datetime
from .metadata_types import MetadataDescription, PropertyDescription, PropertyTypeString, PropertyTypeInt

logger = logging.getLogger(__name__)

class ObjectTracker:
    def __init__(self, tracker_type="CSRT", max_frames_no_detection=60, iou_threshold=0.1, bbox_update_iou_threshold=0.5):
        self.tracker_type = tracker_type
        self.max_frames_no_detection = max_frames_no_detection
        self.iou_threshold = iou_threshold
        self.bbox_update_iou_threshold = bbox_update_iou_threshold
        self.trackers = {}
        self.track_id_counter = 1

    def describe(self):
        return "The OpenCV Tracker component provides object tracking using any of the algorithms supported by OpenCV."

    def describe_properties(self):
        return {
            "tracker_type": PropertyDescription(
                Name="tracker_type",
                Type=PropertyTypeString,
                Description="The type of tracker to run (BOOSTING, CSRT, KCF, MEDIAN_FLOW, MIL, MOSSE, TLD)",
                Default="CSRT"
            ),
            "max_frames_no_detection": PropertyDescription(
                Name="max_frames_no_detection",
                Type=PropertyTypeInt,
                Description="Delete a track if no detections were associated with it for this many frames",
                Default="60"
            ),
            "iou_threshold": PropertyDescription(
                Name="iou_threshold",
                Type=PropertyTypeFloat,
                Description="The intersection over union threshold for track to detection association",
                Default="0.1"
            ),
            "bbox_update_iou_threshold": PropertyDescription(
                Name="bbox_update_iou_threshold",
                Type=PropertyTypeFloat,
                Description="Re-initialize the track if the intersection over union threshold between track and detection falls below this value",
                Default="0.5"
            )
        }

    def create_tracker(self):
        if self.tracker_type == "CSRT":
            return cv2.TrackerCSRT_create()
        elif self.tracker_type == "KCF":
            return cv2.TrackerKCF_create()
        elif self.tracker_type == "MOSSE":
            return cv2.TrackerMOSSE_create()
        elif self.tracker_type == "BOOSTING":
            return cv2.TrackerBoosting_create()
        elif self.tracker_type == "MEDIAN_FLOW":
            return cv2.TrackerMedianFlow_create()
        elif self.tracker_type == "TLD":
            return cv2.TrackerTLD_create()
        elif self.tracker_type == "MIL":
            return cv2.TrackerMIL_create()
        else:
            raise ValueError(f"Unsupported tracker type: {self.tracker_type}")

    def process_frame(self, frame, metadata_map):
        input_channel = frame["config"]["inputs"][0]
        detections = metadata_map.get(input_channel, [])
        new_trackers = []
        for detection in detections:
            bbox = detection["bbox"]
            x, y, w, h = bbox["x1"], bbox["y1"], bbox["x2"] - bbox["x1"], bbox["y2"] - bbox["y1"]
            tracker = self.create_tracker()
            tracker.init(frame["image"], (x, y, w, h))
            new_trackers.append({"id": self.track_id_counter, "tracker": tracker})
            self.track_id_counter += 1
        self.trackers = new_trackers
        return frame

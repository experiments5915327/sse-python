import cv2
import time
import logging
from datetime import datetime
from .mils_client import MILSClient

logger = logging.getLogger(__name__)

class VideoFileReader:
    def __init__(self, file_path, loop=False):
        self.file_path = file_path
        self.loop = loop
        self.video_capture = None
        self.frame_number = 0
        self.start_time = None
        self.cur_time = None

    def open_file(self):
        logger.info("Opening video file: %s", self.file_path)
        self.video_capture = cv2.VideoCapture(self.file_path)
        if not self.video_capture.isOpened():
            raise IOError(f"Cannot open video file {self.file_path}")

    def close_file(self):
        logger.info("Closing video file: %s", self.file_path)
        if self.video_capture:
            self.video_capture.release()

    def get_frame_rate(self):
        return self.video_capture.get(cv2.CAP_PROP_FPS)

    def process(self, out):
        try:
            self.open_file()
            logger.warning("Starting to read frames")
            while True:
                success, img = self.video_capture.read()
                if success:
                    self.frame_number += 1
                    if self.start_time is None:
                        self.start_time = datetime.now()
                        self.cur_time = self.start_time
                    else:
                        ts = self.video_capture.get(cv2.CAP_PROP_POS_MSEC)
                        self.cur_time = self.start_time + timedelta(milliseconds=ts)

                    logger.info("Reading frame %d", self.frame_number)
                    out.put((img, self.frame_number, self.cur_time))
                else:
                    logger.info("End of file")
                    if self.loop:
                        logger.info("Looping, video file will be reopened")
                        self.close_file()
                        self.open_file()
                    else:
                        break
            logger.warning("Done reading frames, %d frames read", self.frame_number)
        except Exception as e:
            logger.error("Error processing video file: %s", e)
        finally:
            self.close_file()

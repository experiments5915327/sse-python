import aiohttp
import cv2
import io
from .logger import get_logger

logger = get_logger(__name__)

async def post_image(url: str, image):
    _, img_encoded = cv2.imencode('.jpg', image)
    img_bytes = io.BytesIO(img_encoded.tobytes())
    async with aiohttp.ClientSession() as session:
        try:
            async with session.post(url, data={"image": img_bytes}) as response:
                response.raise_for_status()
                return await response.json(content_type=None)
        except aiohttp.ClientError as e:
            logger.error(f"HTTP request failed: {e}")
            raise

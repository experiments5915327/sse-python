import json
import logging
import time
import requests
from collections import deque
from datetime import datetime
from typing import List, Dict, Tuple, Any
import cv2
import numpy as np
import asyncio

# Constants
DLEFilterDefaultRate = 5.0
DLEFilterDefaultCropJpegQuality = 95
DLEFilterAggregatorDefaultMinConfidence = 0.5
DLEFilterAggregatorDefaultMaxConfidence = 1.0
DLEFilterAggregatorDefaultMinPositiveRatio = 0.7
DLEFilterAggregatorDefaultMinOverlap = 0.5
DLEFilterAggregatorDefaultObservationWindowSeconds = 5.0
DLEFilterAggregatorDefaultMinSecondsToFilterTriggered = 5.0

logger = logging.getLogger(__name__)

class DLEFilterAggregatorContext:
    def __init__(self, name: str, enable: bool, target_labels: List[str], min_confidence: float, max_confidence: float, 
                 min_overlap: float, observation_window_seconds: float, min_positive_ratio: float, 
                 min_seconds_to_filter_triggered: float):
        self.name = name
        self.enable = enable
        self.target_labels = target_labels
        self.min_confidence = min_confidence
        self.max_confidence = max_confidence
        self.min_overlap = min_overlap
        self.observation_window_seconds = observation_window_seconds
        self.min_positive_ratio = min_positive_ratio
        self.min_seconds_to_filter_triggered = min_seconds_to_filter_triggered
        self.observation_buffer = deque([False] * int(DLEFilterDefaultRate * observation_window_seconds))
        self.last_result = None
        self.start_trigger = None
        self.trigger_time_seconds = -1.0

    def get_positive_ratio(self) -> float:
        positives = sum(self.observation_buffer)
        ratio = positives / len(self.observation_buffer)
        logger.debug(f"Positive count {positives}, buffer length {len(self.observation_buffer)}, ratio = {ratio}")
        return ratio

    def get_trigger_time_seconds(self, cur_time: datetime) -> float:
        if self.start_trigger is None:
            return -1.0  # Not triggered
        return (cur_time - self.start_trigger).total_seconds()

    def is_triggered(self) -> bool:
        return self.trigger_time_seconds >= self.min_seconds_to_filter_triggered


class DLEFilterCrop:
    def __init__(self, x1: float, y1: float, x2: float, y2: float):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2


class DLEFilter:
    def __init__(self, enable: bool, name: str, target_labels: List[str], url: str, api_type_string: str, 
                 rate: float, crop: DLEFilterCrop, crop_jpeg_quality: int, aggregators: List[Dict[str, Any]]):
        self.enable = enable
        self.name = name
        self.target_labels = [label.lower() for label in target_labels]
        self.url = url
        self.api_type_string = api_type_string
        self.rate = rate if rate > 0 else DLEFilterDefaultRate
        self.crop = crop
        self.crop_jpeg_quality = crop_jpeg_quality if crop_jpeg_quality >= 1 else DLEFilterDefaultCropJpegQuality
        self.is_detector = api_type_string.lower() == "detection"
        self.rate_per_second = 1.0 / self.rate
        self.last_updated = datetime.min
        self.dle_client = DLEClient()  # Placeholder for the actual DLE client
        self.aggregator_contexts = self.initialize_aggregators(aggregators)

    def initialize_aggregators(self, aggregators: List[Dict[str, Any]]) -> List[DLEFilterAggregatorContext]:
        aggregator_contexts = []
        for aggregator in aggregators:
            if not aggregator.get("enable", False):
                continue

            context = DLEFilterAggregatorContext(
                name=aggregator.get("name"),
                enable=aggregator.get("enable"),
                target_labels=[label.lower() for label in aggregator.get("target_labels", [])],
                min_confidence=aggregator.get("min_confidence", DLEFilterAggregatorDefaultMinConfidence),
                max_confidence=aggregator.get("max_confidence", DLEFilterAggregatorDefaultMaxConfidence),
                min_overlap=aggregator.get("min_overlap", DLEFilterAggregatorDefaultMinOverlap),
                observation_window_seconds=aggregator.get("observation_window_seconds", DLEFilterAggregatorDefaultObservationWindowSeconds),
                min_positive_ratio=aggregator.get("min_positive_ratio", DLEFilterAggregatorDefaultMinPositiveRatio),
                min_seconds_to_filter_triggered=aggregator.get("min_seconds_to_filter_triggered", DLEFilterAggregatorDefaultMinSecondsToFilterTriggered)
            )
            aggregator_contexts.append(context)

        return aggregator_contexts

    def update(self, frame: 'Frame', detected_object: 'DetectedObject'):
        if not self.enable:
            logger.debug(f"Filter for URL {self.url} is not enabled")
            return

        if detected_object is None:
            logger.error("Detected object is nil")
            return

        if detected_object.extrapolated:
            logger.debug(f"Skipping detected object with id {detected_object.identifier} because it is extrapolated")
            return

        update_filter = (datetime.now() - self.last_updated).total_seconds() >= self.rate_per_second
        if not update_filter:
            logger.debug(f"Filter for URL {self.url} was updated too recently")
            return

        found = any(detected_object.label.name.lower() == label for label in self.target_labels)
        if not found:
            logger.debug(f"Filter for URL {self.url} does not apply to object of type {detected_object.label.name}")
            return

        if self.is_detector:
            filter_results = self.run_detection_filter(frame, detected_object)
        else:
            filter_results = self.run_classification_filter(frame, detected_object)

        self.update_aggregator_contexts(filter_results, detected_object)
        self.last_updated = datetime.now()
        logger.debug(f"Ran filter for URL {self.url} at {self.last_updated}")

    def run_detection_filter(self, frame: 'Frame', detected_object: 'DetectedObject') -> List['DetectedObject']:
        crop, crop_rect = self.crop_image(frame.image, detected_object.rectangle)
        detected_objects = self.dle_client.call_detector(self.url, "myimage", self.crop_jpeg_quality, crop)
        for obj in detected_objects:
            obj.rectangle = self.adjust_rectangle(obj.rectangle, crop_rect)
        return detected_objects

    def run_classification_filter(self, frame: 'Frame', detected_object: 'DetectedObject') -> List['DetectedObject']:
        crop, _ = self.crop_image(frame.image, detected_object.rectangle)
        attributes = self.dle_client.call_classifier(self.url, "myimage", self.crop_jpeg_quality, crop)
        detected_objects = [DetectedObject(identifier=detected_object.identifier, label=Label(name=attr['label']), 
                                           score=attr['score'], rectangle=Rectangle(), extrapolated=False, pose='unknown')
                            for attr in attributes]
        return detected_objects

    def update_aggregator_contexts(self, filter_results: List['DetectedObject'], detected_object: 'DetectedObject'):
        for context in self.aggregator_contexts:
            observed = any(self.match_filter_result(result, detected_object, context) for result in filter_results)
            context.observation_buffer.append(observed)
            context.observation_buffer.popleft()

            positive_ratio = context.get_positive_ratio()
            if positive_ratio < context.min_positive_ratio:
                context.start_trigger = None
                context.trigger_time_seconds = -1.0
            else:
                if context.start_trigger is None:
                    context.start_trigger = datetime.now()
                context.trigger_time_seconds = context.get_trigger_time_seconds(datetime.now())

    def match_filter_result(self, result: 'DetectedObject', detected_object: 'DetectedObject', context: DLEFilterAggregatorContext) -> bool:
        return (result.label.name in context.target_labels and 
                context.min_confidence <= result.score <= context.max_confidence and
                (not self.is_detector or self.get_overlap(result.rectangle, detected_object.rectangle) >= context.min_overlap))

    @staticmethod
    def crop_image(image: np.ndarray, rect: 'Rectangle') -> Tuple[np.ndarray, 'Rectangle']:
        """
        Crop the image to the expanded rectangle defined by the given coordinates.
        """
        height, width, _ = image.shape
        x1 = int(rect.min[0])
        y1 = int(rect.min[1])
        x2 = int(rect.max[0])
        y2 = int(rect.max[1])
        
        crop = image[y1:y2, x1:x2]
        crop_rect = Rectangle((x1, y1), (x2, y2))
        
        return crop, crop_rect

    @staticmethod
    def adjust_rectangle(rect: 'Rectangle', crop_rect: 'Rectangle') -> 'Rectangle':
        """
        Adjust the coordinates of the rectangle based on the crop rectangle.
        """
        rect.min = (rect.min[0] + crop_rect.min[0], rect.min[1] + crop_rect.min[1])
        rect.max = (rect.max[0] + crop_rect.min[0], rect.max[1] + crop_rect.min[1])
        return rect

    @staticmethod
    def get_overlap(rect1: 'Rectangle', rect2: 'Rectangle') -> float:
        """
        Calculate the Intersection over Union (IoU) of two rectangles.
        """
        xA = max(rect1.min[0], rect2.min[0])
        yA = max(rect1.min[1], rect2.min[1])
        xB = min(rect1.max[0], rect2.max[0])
        yB = min(rect1.max[1], rect2.max[1])

        interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

        boxAArea = (rect1.max[0] - rect1.min[0] + 1) * (rect1.max[1] - rect1.min[1] + 1)
        boxBArea = (rect2.max[0] - rect2.min[0] + 1) * (rect2.max[1] - rect2.min[1] + 1)

        iou = interArea / float(boxAArea + boxBArea - interArea)
        return iou


class DLEClient:
    DLE_CLIENT_IMAGE_NAME = "image"
    WVR_CLIENT_IMAGE_NAME = "images_file"

    def __init__(self, base_url: str):
        self.base_url = base_url
        self.http_client = requests.Session()
        self.is_wvr = False
        self.convert_labels_to_lower_case = False
        self.next_id = 1
        self.cookies = {}

    def set_is_wvr(self, is_wvr: bool):
        self.is_wvr = is_wvr

    def set_convert_labels_to_lower_case(self, convert_labels_to_lower_case: bool):
        self.convert_labels_to_lower_case = convert_labels_to_lower_case

    def set_cookies(self, cookies: Dict[str, str]):
        self.cookies = cookies

    def convert_to_jpeg(self, img: np.ndarray, jpeg_quality: int) -> bytes:
        _, jpeg_bytes = cv2.imencode('.jpg', img, [int(cv2.IMWRITE_JPEG_QUALITY), jpeg_quality])
        return jpeg_bytes.tobytes()

    def call_dle(self, filename: str, jpeg_quality: int, img: np.ndarray) -> List[Dict[str, Any]]:
        results = []
        jpeg_bytes = self.convert_to_jpeg(img, jpeg_quality)

        files = {
            self.WVR_CLIENT_IMAGE_NAME if self.is_wvr else self.DLE_CLIENT_IMAGE_NAME: (filename, jpeg_bytes, 'image/jpeg')
        }

        response = self.http_client.post(self.base_url, files=files, cookies=self.cookies)
        if response.status_code != 200:
            logger.warning(f"Bad HTTP response status: {response.status_code}")
            raise RuntimeError(f"Bad HTTP response status: {response.status_code}")

        response_data = response.json()
        logger.debug(f"DLE response: {response_data}")

        if not self.is_wvr and response_data.get('status') != 'OK':
            logger.warning(f"Bad DLE response status: {response_data.get('status')}")
            raise RuntimeError(f"Bad DLE response status: {response_data.get('status')}")

        results_name = "images" if self.is_wvr else "results"
        results = response_data.get(results_name, [])
        return results

    def call_detector(self, filename: str, jpeg_quality: int, img: np.ndarray) -> List['DetectedObject']:
        results = self.call_dle(filename, jpeg_quality, img)
        detections = []
        for result in results:
            for detection in result.get("detections", []):
                bbox = detection["bbox"]
                detected_object = DetectedObject(
                    identifier=self.next_id,
                    label=Label(name=detection["label"].lower() if self.convert_labels_to_lower_case else detection["label"]),
                    score=detection["score"],
                    rectangle=Rectangle(min_point=(bbox["x1"], bbox["y1"]), max_point=(bbox["x2"], bbox["y2"])),
                    extrapolated=False,
                    pose='unknown'
                )
                detections.append(detected_object)
                self.next_id += 1
        return detections

    def get_attributes(self, classifications: List[Dict[str, Any]], min_score: float, max_score: float) -> List['Attribute']:
        attributes = []
        for classification in classifications:
            attr_score = classification["score"]
            if min_score <= attr_score <= max_score:
                attr_label = classification["label"].lower() if self.convert_labels_to_lower_case else classification["label"]
                logger.debug(f"Got attribute {attr_label}={attr_score}")
                attributes.append(Attribute(label=attr_label, score=attr_score))
        return attributes

    def call_classifier_with_score_range(self, filename: str, jpeg_quality: int, img: np.ndarray, min_score: float, max_score: float) -> List['Attribute']:
        results = self.call_dle(filename, jpeg_quality, img)
        if not results:
            return []

        if self.is_wvr:
            return self.get_attributes(results[0].get("classifiers", [])[0].get("classes", []), min_score, max_score)
        else:
            return self.get_attributes(results[0].get("classes", []), min_score, max_score)

    def call_classifier(self, filename: str, jpeg_quality: int, img: np.ndarray) -> List['Attribute']:
        return self.call_classifier_with_score_range(filename, jpeg_quality, img, 0.0, 1.0)
    
    async def call_classifier_async(self, filename: str, jpeg_quality: int, img: np.ndarray) -> List['Attribute']:
        loop = asyncio.get_running_loop()
        return await loop.run_in_executor(None, self.call_classifier, filename, jpeg_quality, img)




class DetectedObject:
    def __init__(self, identifier: int, label: 'Label', score: float, rectangle: 'Rectangle', extrapolated: bool, pose: str):
        self.identifier = identifier
        self.label = label
        self.score = score
        self.rectangle = rectangle
        self.extrapolated = extrapolated
        self.pose = pose

class Rectangle:
    def __init__(self, min_point: Tuple[int, int], max_point: Tuple[int, int]):
        self.min = min_point
        self.max = max_point

class Label:
    def __init__(self, name: str):
        self.name = name

class Attribute:
    def __init__(self, label: str, score: float):
        self.label = label
        self.score = score

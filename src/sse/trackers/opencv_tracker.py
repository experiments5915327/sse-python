import cv2
import logging

logger = logging.getLogger(__name__)

class OpenCVTracker:
    TRACKER_TYPES = {
        "BOOSTING": cv2.legacy.TrackerBoosting_create,
        "MIL": cv2.legacy.TrackerMIL_create,
        "KCF": cv2.legacy.TrackerKCF_create,
        "TLD": cv2.legacy.TrackerTLD_create,
        "MEDIAN_FLOW": cv2.legacy.TrackerMedianFlow_create,
        "CSRT": cv2.TrackerCSRT_create,
        "MOSSE": cv2.legacy.TrackerMOSSE_create
    }

    def __init__(self, tracker_type="CSRT", max_frames_no_detection=60, iou_threshold=0.1, bbox_update_iou_threshold=0.5):
        self.tracker_type = tracker_type
        self.max_frames_no_detection = max_frames_no_detection
        self.iou_threshold = iou_threshold
        self.bbox_update_iou_threshold = bbox_update_iou_threshold
        self.trackers = {}
        self.track_id_counter = 1

    def create_tracker(self):
        if self.tracker_type in self.TRACKER_TYPES:
            return self.TRACKER_TYPES[self.tracker_type]()
        else:
            raise ValueError(f"Unknown tracker type: {self.tracker_type}")

    def add_tracker(self, frame, bbox):
        tracker = self.create_tracker()
        tracker.init(frame, bbox)
        track_id = self.track_id_counter
        self.trackers[track_id] = {
            "tracker": tracker,
            "bbox": bbox,
            "last_detected_frame": 0
        }
        self.track_id_counter += 1
        return track_id

    def update_trackers(self, frame, frame_number):
        to_remove = []
        for track_id, track_info in self.trackers.items():
            success, bbox = track_info["tracker"].update(frame)
            if success:
                track_info["bbox"] = bbox
                track_info["last_detected_frame"] = frame_number
            else:
                if frame_number - track_info["last_detected_frame"] > self.max_frames_no_detection:
                    to_remove.append(track_id)
        for track_id in to_remove:
            del self.trackers[track_id]

    def get_trackers(self):
        return self.trackers

    def reset(self):
        self.trackers = {}
        self.track_id_counter = 1

    @staticmethod
    def compute_iou(boxA, boxB):
        xA = max(boxA[0], boxB[0])
        yA = max(boxA[1], boxB[1])
        xB = min(boxA[0] + boxA[2], boxB[0] + boxB[2])
        yB = min(boxA[1] + boxA[3], boxB[1] + boxB[3])
        
        interArea = max(0, xB - xA) * max(0, yB - yA)
        
        boxAArea = boxA[2] * boxA[3]
        boxBArea = boxB[2] * boxB[3]
        
        iou = interArea / float(boxAArea + boxBArea - interArea)
        
        return iou

import cv2
import logging
from datetime import datetime
from .metadata_types import MetadataDescription, PropertyDescription, PropertyTypeString, PropertyTypeInt

logger = logging.getLogger(__name__)

class TrackRenderer:
    def __init__(self):
        pass

    def describe(self):
        return "The track renderer component consumes the tracked objects associated with the frame and draws each object's track directly on the frame image."

    def describe_properties(self):
        return {}

    def process_frame(self, frame, metadata_map):
        input_channel = frame["config"]["inputs"][0]
        tracks = metadata_map.get(input_channel, [])
        for track in tracks:
            detected_object = track["DetectedObject"]
            bbox = detected_object["Rectangle"]
            label = detected_object["Label"]
            color = (0, 255, 0)  # Green color
            cv2.rectangle(frame["image"], (bbox["x1"], bbox["y1"]), (bbox["x2"], bbox["y2"]), color, 2)
            cv2.putText(frame["image"], label, (bbox["x1"], bbox["y1"] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
        return frame

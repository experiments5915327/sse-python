import os

RTSP_STREAMS = []
DETECTION_INTERVAL = float(os.getenv("DETECTION_INTERVAL", 1.0))
DLE_PYTHON_URL = os.getenv("DLE_PYTHON_URL", "http://localhost:5000/detect/objects")
LOG_LEVEL = os.getenv("LOG_LEVEL", "INFO")
IMAGE_WIDTH = int(os.getenv("IMAGE_WIDTH", 640))
IMAGE_HEIGHT = int(os.getenv("IMAGE_HEIGHT", 480))
IMAGE_LOG_FOLDER = os.getenv("IMAGE_LOG_FOLDER", "/app/image_log")
RATE_LIMIT = float(os.getenv("RATE_LIMIT", 4.0))  # seconds per request to dle-python
CONFIG_FILE_PATH = os.getenv("CONFIG_FILE_PATH", "/app/config/streams.json")

# MILS Configuration
MILS_URL = os.getenv("MILS_URL", "https://mils-host")
MILS_USER = os.getenv("MILS_USER", "username")
MILS_PASSWORD = os.getenv("MILS_PASSWORD", "your-password")
MILS_LOCALE = os.getenv("MILS_LOCALE", "en_US")
MILS_TIMESTAMP_FORMAT = os.getenv("MILS_TIMESTAMP_FORMAT", "yyyy-MM-dd'T'HH:mm:ss.SSSSSS")
MILS_TIMEZONE = os.getenv("MILS_TIMEZONE", "America/New_York")
MILS_VERIFY_SSL = os.getenv("MILS_VERIFY_SSL", "true").lower() in ["true", "1", "yes"]

# SSE Configuration
SSE_HOST = os.getenv("SSE_HOST", "localhost")
SSE_HOST_PORT = int(os.getenv("SSE_HOST_PORT", 8000))
VIEW_ID = int(os.getenv("VIEW_ID", 1))
STATE = int(os.getenv("STATE", 1))
VFI = os.getenv("VFI", "true").lower() in ["true", "1", "yes"]

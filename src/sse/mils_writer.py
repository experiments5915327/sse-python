class MILSWriter:
    def __init__(self, mils_client):
        self.mils_client = mils_client

    def process_frame(self, frame):
        start_time = time.time()
        input_channel = frame["config"]["inputs"][0]
        metadata_pointers = frame["metadata_map"].get(input_channel, [])

        if metadata_pointers:
            logger.info(f"Sending metadata from frame {frame['sequence']} to MILS")
            for alert_untyped in metadata_pointers:
                alert = alert_untyped if isinstance(alert_untyped, dict) and alert_untyped.get("metadata_type") == "alert" else None
                if alert is None:
                    logger.error(f"Metadata of type {alert_untyped.get('metadata_type')} is not an alert")
                    continue
                # send alert to MILS
                err = self.mils_client.send_alert(alert, frame["image"].shape[0])
                if err:
                    logger.error(f"Failed to send alert to MILS: {err}")

        logger.info(f"Processed frame {frame['sequence']} in {time.time() - start_time:.2f} seconds")
import cv2
import numpy as np
from queue import Queue
from threading import Thread
from typing import Tuple, List

class VideoStabilizer:
    def __init__(self):
        self.prev_gray = None
        self.prev_to_cur_transform = np.eye(3)
        self.transforms = []
        self.frame_queue = Queue(maxsize=30)
        self.stabilized_frame_queue = Queue(maxsize=30)
        self.processing_thread = Thread(target=self._process_frames)
        self.running = False

    def start(self):
        self.running = True
        self.processing_thread.start()

    def stop(self):
        self.running = False
        self.processing_thread.join()

    def add_frame(self, frame: np.ndarray):
        self.frame_queue.put(frame)

    def get_stabilized_frame(self) -> np.ndarray:
        return self.stabilized_frame_queue.get()

    def _process_frames(self):
        while self.running or not self.frame_queue.empty():
            try:
                frame = self.frame_queue.get(timeout=1)
            except Queue.Empty:
                continue

            if self.prev_gray is None:
                self.prev_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                self.stabilized_frame_queue.put(frame)
                continue

            cur_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            prev_pts = cv2.goodFeaturesToTrack(self.prev_gray, maxCorners=100, qualityLevel=0.01, minDistance=30, blockSize=3)

            if prev_pts is not None:
                cur_pts, status, err = cv2.calcOpticalFlowPyrLK(self.prev_gray, cur_gray, prev_pts, None)
                idx = np.where(status == 1)[0]
                prev_pts = prev_pts[idx]
                cur_pts = cur_pts[idx]

                m, inliers = cv2.estimateAffinePartial2D(prev_pts, cur_pts)
                dx, dy = m[0, 2], m[1, 2]
                da = np.arctan2(m[1, 0], m[0, 0])

                transform = np.array([
                    [np.cos(da), -np.sin(da), dx],
                    [np.sin(da), np.cos(da), dy],
                    [0, 0, 1]
                ])

                self.prev_to_cur_transform = self.prev_to_cur_transform @ transform
                self.transforms.append(self.prev_to_cur_transform)

                stabilized_frame = cv2.warpAffine(frame, self.prev_to_cur_transform[:2], (frame.shape[1], frame.shape[0]))
                self.stabilized_frame_queue.put(stabilized_frame)

                self.prev_gray = cur_gray

    def get_transformations(self) -> List[np.ndarray]:
        return self.transforms
